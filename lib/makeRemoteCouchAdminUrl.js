const _ = require("underscore");
const $$ = require("squeak/node");
require("squeak/extension/url");
let $$log = $$.logger("dato");
const fetch = require("node-fetch");
const appConfig = require("../boot/config");
let autosynchronizationSettings = require("./autosynchronizationSettings");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  MAKE COUCH ADMIN URL
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

function makeRemoteCouchAdminUrl (couchUrl) {
  if (appConfig.couchAdminAuthentication) {

    // username and password set
    if (_.isString(appConfig.couchAdminAuthentication.username) && _.isString(appConfig.couchAdminAuthentication.password)) {
      var decomposedCouchUrl = $$.url.decompose(couchUrl);
      return $$.url.make($$.defaults(decomposedCouchUrl, appConfig.couchAdminAuthentication));
    }

    // something missing
    else $$log.error("The 'couchAdminAuthentication' setting is not properly setup in boot/config. Check help/readme page for more details on what values are allowed.");

  };
};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  VERIFY COUCH ADMIN URL IS WORKING
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

/**
  DESCRIPTION: make a request to couchdb to verify that the url and admin username and password are right
  ARGUMENTS: (
    !remoteCouchAdminUrl <string> « couch url with admin username and password »,
    !callback <function(ø):<void>> « callback is ran only if success »,
  )
  RETURN: <void>
*/
function verifyCouchAdminUrlIsWorking (remoteCouchAdminUrl, callback) {

  fetch(remoteCouchAdminUrl +"_users/_all_docs")
    .then((fetchResponse) => fetchResponse.json())
    .then((couchResponse) => {
      if (couchResponse.error) $$log.error(
        "Failed to verify couch admin url, there must be an error in the chosen couch url, or admin username and/or password:",
        couchResponse
      );
      else {
        $$log.success("Successfully verified couch admin url.");
        callback();
      };
    })
    .catch((err) => {
      $$log.error("Request error trying to verify couch admin url:", err);
    })
  ;

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

let couchAdminUrl;
/**
  DESCRIPTION: make remote couchdb url
  ARGUMENTS: (
    !callback <function(couchAdminUrl<string>)> « fired after remoteCouchAdminUrl has been checked, but only if a valid url with valid admin username and password have been set »
  )
  RETURN: <void>
*/
function setupAutoSynchronization (callback) {

  // IF NO COUCH ADMIN AUTHENTICATION, JUST DO NOTHING
  if (!appConfig.couchAdminAuthentication) return;

  // IF NO COUCH URL SET, CANCEL NOW AND LOG ERROR
  if (!$$.getValue(appConfig, "authentication.couchUrl")) {
    $$log.error("To setup autosynchronization of databases, you must also enable authentication.");
    return
  };

  // get couchUrl from appConfig.couchAdminAuthentication.customCouchUrl or from appConfig.authentication.couchUrl
  var remoteCouchAdminUrl = $$.getValue(appConfig, "couchAdminAuthentication.customCouchUrl") || appConfig.authentication.couchUrl;
  if (!remoteCouchAdminUrl || !_.isString(remoteCouchAdminUrl)) return $$log.error("Cannot setup remote couch admin url. If you want to use it, you must set an authentication url in the 'config/private.js' file.")
  else if (!remoteCouchAdminUrl.match(/\/$/)) remoteCouchAdminUrl = remoteCouchAdminUrl +"/";

  // make remote couch url with password
  let remoteCouchAdminUrlWithPassword = makeRemoteCouchAdminUrl(remoteCouchAdminUrl);
  autosynchronizationSettings.calculated = true;

  // async, but it shouldn't really matter for the following
  if (remoteCouchAdminUrlWithPassword) verifyCouchAdminUrlIsWorking(remoteCouchAdminUrlWithPassword, function () {
    autosynchronizationSettings.status = "enabled";
    autosynchronizationSettings.url = remoteCouchAdminUrl;
    couchAdminUrl = remoteCouchAdminUrlWithPassword;
    if (callback) callback(remoteCouchAdminUrlWithPassword);
    if (remoteCouchAdminUrlWithPassword) $$log.success("Autosynchronization has been enabled.");
  });

};

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

setupAutoSynchronization();
module.exports = function () { return couchAdminUrl; };
