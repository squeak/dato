
/*

  All the following libraries are available by default globally in this script and pages scripts:
    _ = underscore
    $ = yquerj (a version of jquery with a few more utilities)
    $$ = squeak

  Put everything you want to display in $app (it's a jquery object: $app = $("#electrode-app")).

  This script will be executed first in all pages.

*/

// DISABLE PINCH ZOOMING
// $("head").append('<meta name="viewport" content="width=device-width, user-scalable=no">');
if ($$.isTouchDevice()) $("head").append('<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />');
