# dato

Dato is a deeply customizable app to create lists and databases.

For the full documentation, installation instructions... check [dato documentation page](https://squeak.eauchat.org/dato/).

To try a running public instance of dato, visit [this page](https://publicdato.eauchat.org/).
