const path = require("path");

module.exports = {

  //
  //                              GENERAL

  name: "dato",
  // you can customize the port dato is running on
  port: 9559,
  // colors of the "animation" in terminal on server start
  logColor: ["#FFFF17", "#FFD703", "#FFA217", "#FF4929", "#FF1717"],

  appPath: path.join(__dirname +"/../"), // Important, don't change this unless you move this script.

  // set to false to recalculate pages' dist files at each request (useful to quickly test changes you'd make to the code)
  // ⚠️ if you set this to false, service worker caching of pages may not work properly
  production: true,

  // set this to true if you want more debug logs
  debug: false,

  // calculate list of submodules to allow to easily find less style files in submodules <false|"always"|"once">
  resolveSubmodulesPathsRecursivelyForLessImportStatments: "once",

  // by default, set HSTS header to 90 days
  pageHeaders: {
    "Strict-Transport-Security": "max-age=7776000",
  },

  // setup caching of the app for offline usage (this setup allows the user to use dato even when they are offline)
  pwa: {
    cache: true,
    pages: [
      "/",
      "/error/",
      // login, users and user page are not in here since they should only be available when the user is online
    ],
    customUrls: [
      "/databases/autosync-settings/",
      "/share/contributopia.jpg",
      "/share/d.svg",
      "/share/dato.svg",
      "/share/help/inputify-label.png",
      "/dato-recent-changelog/",
      // if you customize medias in the page, don't forget to add them to this list, so they are cached for offline use
    ],
    manifest: {
      icons: [
        {
          "src": "/share/favicon.png",
          "sizes": "200x200",
          "type": "image/png"
        },
        {
          "src": "/share/d.svg",
          "sizes": "1000x1000",
        },
        {
          "src": "/share/dato.svg",
          "sizes": "3014x1043",
        },
      ],
      display: "fullscreen", // "fullscreen, ""standalone", "minimal-ui", or "browser"
      theme_color: "#FFA217",
      background_color: "#FFE96F", // #FF4929
    },
  },

  //
  //                              AUTHENTICATION

  authentication: {
    _recursiveOption: true,
    leavePasswordInUserProfile: true,
    logoutRedirect: "/login/?status=loggedOut",
    // unquote the following (and customize if necessary) if you don't want the app to be public, or/and if you want to enable autosynchronization
    // choose here the url to the couchdb server containing your list of users (if you enable autosynchronization this couchdb server will also be used to store users databases)
    // couchUrl: "http://localhost:5984/",
  },

  //
  //                              ENABLING AUTOSYNC

  /**
    DESCRIPTION:
      if you want users to have an optional autosynchronization of their databases with this couchdb server, you can set this option
      it gives dato access to this database as administrator, so that when a user creates/removes a database, dato can automatically create/remove databases in the server
      ⚠⚠⚠ you must have set the previous authentication.couchUrl to a valid url for couchAdminAuthentication to be used
    TYPES:
      couchAdminAuthentication = <
        |undefined « if couchAdminAuthentication is undefined, users won't have any default synchronization setup of their databases (means they will only be saved in their browser locally) »,
        |credentials « users databases will by default be setup to synchronize automatically with this couch server (it's always disablable by the user in each database's settings) »,
      >
      credentials = <{
        !username: <string> « username of admin in couch »,
        !password: <string> « password for this admin account »,
        ?customCouchUrl: <string> «
          if you chose a local url to verify authentication, you should define here a public url that a user can access your database with,
          you may use a different database server as the one used for authentication, but if you do so, this other server must have the same users list than the one used for authentication to dato
        »,
      }>
  */
  // couchAdminAuthentication: {
  //   username: "couchadmin",
  //   password: "some_pass",
  //   customCouchUrl: "https://yourCouchServer.tld/",
  // },

  //                              ¬
  //

};
