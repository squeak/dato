const fs = require("fs");
const path = require("path");
const _ = require("underscore");
const $$ = require("squeak");
let $$log = $$.logger("dato");
const appPath = path.join(__dirname +"/../"); // Important, don't change this unless you move this script.

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  MAKE SURE NECESSARY ADDITIONAL CONFIG FILES EXIST
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

if (!fs.existsSync(appPath +"/global/theme.less")) {
  $$log.success("Generated default theme stylesheet.");
  fs.writeFileSync(appPath +"/global/theme.less", "");
};

if (!fs.existsSync(appPath +"/config/")) {
  $$log.success("Created config folder.");
  fs.mkdirSync(appPath +"/config/");
};

if (!fs.existsSync(appPath +"/config/public.js")) {
  $$log.success("Generated default public config.");
  var publicConfigContent = [
    '',
    'module.exports = {',
    '',
    '  // address to request for accounts or if there is issues with dato (showed in login page)',
    '  // adminEmail: "datoadmin@yourserver.tld",',
    '',
    '  // some other options proposed to the user if they can\'t login (showed in login page, below login form, can be long because it\'s shown only if the user clicks on "other option")',
    '  // otherOptionsThanLogin: "Send a pigeon request to my house and I\'ll create an account for you :)<br>My address is:<br>9559 avenue Poseidon<br>Atlantis",',
    '',
    '};',
  ].join("\n");
  fs.writeFileSync(appPath +"/config/public.js", publicConfigContent);
};

if (!fs.existsSync(appPath +"/config/private.js")) {
  $$log.success("Generated default private config.");
  var privateConfigContent = [
    '',
    'module.exports = {',
    '',
    '  // unquote the following and adapt it if needed, to enable authentication',
    '  // authentication: {',
    '  //   couchUrl: "http://localhost:5984/",',
    '  // },',
    '',
    '  // unquote and fill appropriately the following to enable autosynchronization of the databases',
    '  // couchAdminAuthentication: {',
    '  //   username: "adminUsername",',
    '  //   password: "adminPasswørd",',
    '  //   customCouchUrl: "https://myCouchDomain.tld/",',
    '  // },',
    '',
    '};',
  ].join("\n");
  fs.writeFileSync(appPath +"/config/private.js", privateConfigContent);
};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

const defaultConfig = require("./defaultConfig");
const passedPrivateConfig = require("../config/private");

module.exports = $$.defaults(defaultConfig, passedPrivateConfig);
