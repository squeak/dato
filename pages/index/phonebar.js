var _ = require("underscore");
var $$ = require("squeak");
var windify = require("windify");

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

module.exports = function (spacified, globalSettings) {
  return [
    //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
    //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

    // GO TO HOME
    {
      name: "goToHome",
      icomoon: "home",
      title: "go to home",
      click: function () {
        globalSettings.visitHomeDatabase();
      },
    },

    // DISPLAY SWITCHER
    {
      name: "displaySwitcher",
      icomoon: "grid3",
      title: "display db switcher",
      click: function () {
        spacified.switcher.show();
      },
    },

    // TOGGLE DISPLAY OF WINDIFY WINDOWS
    {
      name: "windifiesDisplay",
      icomoon: "window-minimize", // window-maximize, window-resize, times-rectangle-o
      title: "toggle display of windows",
      clickSelect: function () {
        $app.addClass("dato-hidden_windifies");
        window.datoActualizePhonebarButton("editSelected", spacified.currentDatoSpace.tablifyApp);
      },
      clickUnselect: function () {
        $app.removeClass("dato-hidden_windifies");
        window.datoActualizePhonebarButton("editSelected", spacified.currentDatoSpace.tablifyApp);
      },
    },

    // EDIT SELECTED ENTRIES
    {
      name: "editSelected",
      icomoon: "quill", // edit, plus3
      title: "edit currently selected entries",
      click: function (e) {
        var tablifyApp = spacified.currentDatoSpace.tablifyApp;

        // save currently open entry
        if (tablifyApp.$el.find(".postify").length && !$app.hasClass("dato-hidden_windifies")) {
          var windified = windify.getActiveWindow();
          var saveButton = _.findWhere(windified.options.buttons, { name: "save" });
          if (saveButton) saveButton.click();
        }
        // edit selected entry
        else if (tablifyApp.selected && tablifyApp.selected.length) tablifyApp.editSelectedEntries()
        // create new entry
        else tablifyApp.createNewEntry();
        
      },
    },

    //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
    //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  ];
};
