require("squeak/extension/storage");
require("squeak/extension/url");
var uify = require("uify");
var keyboardify = require("keyboardify");
var globalSettings = require("./utilities/globalSettings");
var electrodeLogout = require("electrode/lib/logout");
var changeSpace = require("./utilities/changeSpace");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  DISPLAY NOTIFICATIONS HISTORY
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

function alertNotificationsHistory () {

  // get toasts history
  var toastsHistory = uify.toast.history();

  // alert toasts history
  uify.dialog({

    // DIALOG TITLE
    title: "Notifications history",

    // CANCEL AND CLEAR HISTORY BUTTONS
    buttons: [
      // clear history button
      {
        title: "clear history",
        click: function () {
          uify.confirm({
            content: "Sure you want to clear notifications history?",
            type: "warning",
            ok: function () {
              uify.toast.clearHistory();
              uify.toast({
                message: "Notifications history cleared",
                type: "success",
                historify: false,
              });
            },
          });
        },
      },
      // cancel button
      "ok",
    ],

    // HISTORY CONTENT
    contentClass: "dato-notifications_history",
    content: function ($container) {

      function showToastsHistory (from, amount) {
        // calculate target index
        var to = from + amount;
        // display notifications
        var historySlice = toastsHistory.slice(from, to);
        _.each(historySlice, function (toastNotif) {
          var $toastMessage = $container.div({
            class: "notif_content notif_type-"+ toastNotif.options.type,
            htmlSanitized: toastNotif.message +'<br><i class="notif_date">'+ toastNotif.date +'</i>',
          });
        });
        // display "more history" button
        if (toastsHistory.length > to) var $moreHistory = $container.span({
          class: "show_more_button",
          text: "+ show "+ (amount*2) +" more +",
        }).click(function () {
          $moreHistory.remove();
          showToastsHistory(to+1, (amount*2));
        });
      };

      // display last 50 notifications
      showToastsHistory(0, 50);

    },

  });

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  SWITCHER BUTTONS
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

var switcherButtons = [
  {
    icomoon: "home",
    title: "open home database",
    key: ["shift + h", "shift + 1"],
    click: function () {
      changeSpace(spacified, "_home");
    },
  },
  {
    icomoon: "library",
    title: "open types database",
    key: ["shift + t", "shift + 2"],
    click: function () {
      changeSpace(spacified, "_types");
    },
  },
  {
    icomoon: "gears",
    title: "edit global settings",
    key: "shift + ,",
    class: "global-settings-button", // custom class to positionize the button differently in page
    click: function () {
      globalSettings.edit();
    },
  },
  {
    icomoon: "history",
    title: "check notifications history",
    key: "shift + p",
    class: "notifications-history-button", // custom class to positionize the button differently in page
    click: alertNotificationsHistory,
  },
  {
    icomoon: "exit", // page.authenticatedUser ? page.authenticatedUser.icon : "",
    title: "logout from dato",
    class: "dato-logout-button", // custom class to positionize the button differently in page
    key: "shift + q",
    // show button
    //   - if authenticated in dato
    //   - or in publicdato if some credentials are stored in local or session storage
    condition: function () { return page.authenticatedUser || $$.storage.session("tablifyCredentials")  || $$.storage.local("tablifyCredentials"); },
    click: function () {
      if (page.authenticatedUser) uify.confirm({
        content: "Logout now?",
        ok: function () {
          // clear sessionStorage, to make sure not saved remote db credentials (for example) stays after logging out
          sessionStorage.clear();
          // logout using electrode logout (that automatically unregister service workers, this is necessary otherwise internal urls are still accessible after logging out, but they should not)
          electrodeLogout();
        },
      })
      else uify.confirm({
        content: "Forget sync credentials?",
        ok: function () {
          // clear sessionStorage and localStorage tablify credentials, to make sure not saved remote db credentials (for example) stays after logging out
          sessionStorage.clear();
          $$.storage.local("tablifyCredentials", null);
          // logout from all sync processes of all dbs
          _.each(spacified.spaces, function (space) { space.tablifyApp.syncLogout(); });
        },
      })
    },
  },
  {
    key: "shift + #",
    css: { display: "none", }, // hide button, this is just to make a keyboard shortcut
    click: function () {
      spacified.visitLastVisited();
    },
  },
];

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

var spacified = uify.spaces({

  $target: $app,
  hideRightArrow: true,
  hideLeftArrow: true,
  switcher: {
    title: "Currently opened databases:",
    buttons: switcherButtons,
    alwaysShowThumbnails: true,
    // do not display home and types dbs in the spaces switcher
    spaceFilter: function (spaceObject) {
      return spaceObject.id != "_home" && spaceObject.id != "_types";
    },
  },
  visitCallback: function (visitedSpace) {
    spacified.currentDatoSpace = visitedSpace;
    // make sure to set shortcuts context
    keyboardify.setContext(spacified.currentDatoSpace.id);
    // if visited space has another hash than current page's one, change page's hash
    if ($$.url.hash.getAsString() != spacified.currentDatoSpace.hash) $$.url.hash.set(spacified.currentDatoSpace.hash);
    // refocus on last focused window on this space
    var toppestWindow = _.max(spacified.currentDatoSpace.$el.find(".windify"), function (elem) { return $(elem).css("z-index"); });
    if (toppestWindow) $(toppestWindow).mousedown();
  },
  removeCallback: function (removedSpace) {
    var databaseIndex = _.findIndex(globalSettings.allDatobases, function (datobject) {
      return datobject.spaceUUID == removedSpace.tablifyUUID;
    });
    // remove datobase from list of currently open datobases
    var closedDatabase = globalSettings.allDatobases.splice(databaseIndex, 1);
    // cancel tablify synchronization of the removed database
    closedDatabase[0].tablifyApp.syncCancel();
  },
});

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  OPEN DATABASE SHORTCUT
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

/**
  DESCRIPTION: open a database and or entries
  ARGUMENTS: (
    !dbName <string>,
    ?entriesToView <string[]> « array of ids of entries to view »,
    ?entriesToEdit <string[]> « array of ids of entries to edit »,
  )
  RETURN: <void>
*/
spacified.datopenDatabase = function (dbName, entriesToView, entriesToEdit) {
  changeSpace(spacified, dbName, function (datobject) {

    // view entries
    _.each(entriesToView, function (entryId) {
      datobject.tablifyApp.openEntry(datobject.tablifyApp.Collection.get(entryId));
    });

    // edit entries
    _.each(entriesToEdit, function (entryId) {
      datobject.tablifyApp.openEntryEditor(datobject.tablifyApp.Collection.get(entryId));
    });

  });
};

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

module.exports = spacified;
