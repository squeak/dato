var displayGlossary = require("./glossary");
var displayCredits = require("./credits");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

module.exports = {

  //
  //                              DATO BUTTON

  dato: function (buttons) {
    return {
      title: "database actions",
      icomoon: "dato", // align-justify, exchange, share2
      position: "bottom",
      positionizeFirst: true,
      key: "shift + ;",
      keyid: "dato-db_actions",
      clickMenu: {
        buttons: buttons,
      },
    };
  },

  //
  //                              SPACES SWITCHER BUTTON

  spacesSwitcher: function () {
    return {
      icomoon: "grid3",
      title: "display navigation panel",
      class: "spaces-switcher-button",
      key: "shift + space",
      keyid: "dato-navigation_panel",
      position: "bottom",
      positionizeFirst: true,
      click: function () {
        var tablifyApp = this;
        tablifyApp.datobject.spaceObject.spacified.switcher.show();
      },
    };
  },

  //
  //                              RELOAD DATABASE

  reloadDatabase: function () {
    return {
      title: "reload this database",
      icomoon: "spinner11",
      key: "r",
      position: "bottom",
      positionizeFirst: true,
      click: function () {
        var tablifyApp = this;
        tablifyApp.datobject.reloadDatabase();
      },
    };
  },

  //
  //                              GLOSSARY

  glossary: function () {
    return {
      title: "glossary", // underine position under because on a "g" underline is not visible otherwise
      icomoon: "etsy",
      key: "g",
      click: function () { displayGlossary(); },
    };
  },

  //
  //                              DEVELOPPERS DOCUMENTATION

  developpersDoc: function () {
    return {
      icomoon: "code",
      title: "open dato's developper documentation",
      click: function (e) { $$.open("https://squeak.eauchat.org/dato/", true) },
    };
  },

  //
  //                              ISSUES PAGE

  issuesPage: function () {
    return {
      icomoon: "gitlab",
      title: "submit an issue or request a feature",
      key: "i",
      click: function (e) { $$.open("https://framagit.org/squeak/dato/issues/", true) },
    };
  },

  //
  //                              CREDITS

  credits: function () {
    return {
      title: "credits and changelog",
      icomoon: "copyright",
      key: "c",
      click: function (e) { displayCredits(); },
    };
  },

  //                              ¬
  //

};
