require("squeak/extension/ajax");
var uify = require("uify");
var loginAgain = require("./login");
var $$log = $$.logger("dato");

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

var autosyncMethods = {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  GET SETTINGS
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: get autosync settings
    ARGUMENTS: (
      callback(<unfefined|{
        calculated: <boolean>,
        status: <"enabled"|"disabled">,
        ?url: <string>,
      }>)
    )
    RETURN: <void>
  */
  getSettings: function (callback) {
    $$.ajax.get({
      url: "/databases/autosync-settings/",
      success: callback,
      error: function (err) {
        var responseError = err.responseJSON ? err.responseJSON : err;

        // if not logged in, propose to log in
        if (responseError && responseError.status) loginAgain(responseError.status, function () {
          // on login success, reask to get settings and callback
          autosyncMethods.getSettings(callback);
        }, errorCallback)

        // other error
        else errorCallback(responseError);

        function errorCallback (errMess) {
          $$log.error(
            "Failed to fetch information on autosync status. Started app with autosync disabled.",
            errMess
          );
          callback();
        };

      },
    });
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: check if the given string is a url or just a db name
    ARGUMENTS: ( ?string <string> )
    RETURN: <boolean>
  */
  isUrl: function (string) {
    // if remote name is a full url, ignore it
    try { new URL(remoteName); return true; }
    // else ok, keep it
    catch (e) { return false; };
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  MAKE DATABASE URL
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  makeDatabaseUrl: function (autosyncCouchUrl, dbName) {
    return autosyncCouchUrl + page.authenticatedUser.name +"--"+ dbName;
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  CREATE DATABASE IN REMOTE COUCH
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: ask backend to create a database in remote couch
    ARGUMENTS: (
      !dbName <string>,
      !silentIfExistAlready <boolean> « if true, will not toast a warning if couch db already exist »,
      ?createdCallback <function(ø)> « executed when successfully created the database (only if created, not if it exists already) »,
    )
    RETURN: <void>
  */
  createDatabase: function (dbName, silentIfExistAlready, createdCallback) {

    $$.ajax.post({
      url: "/databases/create?db="+ dbName,
      success: function (response) {
        if (response.ok) {
          if (response.message != "exist-already") {
            uify.toast.success("Created remote database '"+ dbName +"' successfully.");
            if (createdCallback) createdCallback();
          }
          else if (!silentIfExistAlready) uify.toast("Skipped creating remote database '"+ dbName +"', it exists already.");
        }
        else toastError(response);
      },
      error: function (err) {
        var responseError = err.responseJSON ? err.responseJSON : err;

        // if not logged in, propose to log in
        if (responseError && responseError.status) loginAgain(responseError.status, function () {
          // on login success, reask to create database and callback
          autosyncMethods.createDatabase(dbName, silentIfExistAlready, createdCallback);
        }, toastError)

        // if offline, just log it and keep it for later, otherwise toast an error
        else if (err.status) toastError(err)
        else $$log.info("[dato] Cannot create remote database '"+ dbName +"' at this moment, you are probably offline.");

      },
    });

    function toastError (err) {
      uify.toast.error("Failed to create remote database '"+ dbName +"'.<br>See logs for more details.");
      $$log.error(
        "Failed to create remote database '"+ dbName +"'.",
        err
      );
    };

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  REMOVE DATABASE IN REMOTE COUCH
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  removeDatabase: function (dbName) {

    $$.ajax.post({
      url: "/databases/remove?db="+ dbName,
      success: function (response) {
        if (!response.ok) toastError(response)
        else if (response.status == "not_found") uify.toast("No need to remove inexistant remote database '"+ dbName +"'.")
        else uify.toast("Removed remote database '"+ dbName +"' successfully.");
      },
      error: function (err) {

        // if not logged in, propose to log in
        var responseError = err.responseJSON;
        if (responseError && responseError.status) loginAgain(responseError.status, function () {
          // on login success, reask to get settings and callback
          autosyncMethods.removeDatabase(dbName);
        }, toastError)

        // other error
        else if (err.status) toastError(err)

        // offline toast message
        else uify.toast.error({
          message: "Could not remove remote database '"+ dbName +"', you are probably offline. Please do it manually when you're back online.",
          duration: 0,
        });

      },
    });

    function toastError (err) {
      uify.toast.error({
        message: "Failed to remove remote database '"+ dbName +"'.<br>See logs for more details.",
        duration: 15,
      });
      $$log.error(
        "Failed to remove remote database '"+ dbName +"'.",
        err
      );
    };

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  METHODS TO ATTACH TO AUTOSYNC SETTINGS
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    All the following methods will be attached to autosync settings.
  */
  methods: {

    //
    //                              IS ENABLED

    /**
      DESCRIPTION: check from the settings and authentication state if autosynchronization is possible
      ARGUMENTS: ( ø )
      RETURN: <boolean>
    */
    isEnabled: function (autosyncSettings) {
      var autosyncSettings = this;
      return !autosyncSettings.error && autosyncSettings.status == "enabled" && page.authenticatedUser;
    },

    /**
      DESCRIPTION:
        check if autosync is enabled,
        create a remote db if necessary,
        and reinitialize synchronization in tablify app if necessary
      ARGUMENTS: ( tablifyApp )
      RETURN: <void>
    */
    createRemoteDatabaseIfNecessary: function (tablifyApp) {
      var autosync = this;

      if (autosync.isEnabled()) _.chain(tablifyApp.syncProcesses).filter(function (syncProcess) {
        return syncProcess.remoteConfig.location == "here";
      }).each(function (syncProcess) {
        autosyncMethods.createDatabase(syncProcess.remoteConfig.name, true, function () {
          syncProcess.resetPouchSync();
        });
      });

    },

    //                              ¬
    //

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};

module.exports = autosyncMethods;
