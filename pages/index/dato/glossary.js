require("squeak/extension/string");
var uify = require("uify");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  GLOSSARY WORDS
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

function wordsList () {
  return [

    //
    //                              DATABASE

    {
      word: "Database",
      definition: "A database is a container for entries, it can be synchronized to some server(s), in order to backup the data somewhere, or to make it accessible to many devices.",
    },

    //
    //                              ENTRY

    {
      word: "Entry",
      definition: [
        "An entry is an object that contains a list of key-value pairs.",
        'For example: the key "name" could have the value "Alberto", the key "birthday" could have the value "1489-04-09".',
        "You could then write the entry like this:",
        "<pre>"+ $$.string.htmlify($$.json.pretty({ name: "Alberto", birthday: "1489-04-09", })) +"</pre>",
        "An entry can also contain attachments. An attachment can be any type of file (image, video...).",
      ],
    },

    //
    //                              ENTRY TYPE

    {
      word: "Entry type",
      definition: [
        'An entry type, is the configuration that describes how an entry should be displayed when it\'s viewed, what list of inputs should be proposed when editing it.',
        'For example, a "contact" entry type, could describe that when you edit a contact entry, it will display "name", "phone numbers", "emails"... fields. It could also describe that when you open a contact, you will see the list of phone numbers of this contact, and you can click on the number to call that person... All this kind of things.',
        'You can create any ammount of entry types from the list of "types" (that you can open clicking on <span class="icon-grid3"></span> in the right toolbar, and then <span class="icon-library"></span>).',
      ],
    },

    //
    //                              VALUE TYPE

    {
      word: "Value type",
      definition: [
        "A value in an entry can be of various types.",
        "Here is a list of basic javascript types:",
        makeTable([
          [
            "type",
            "description",
          ],
          [
            "string",
            "A string, is a chain or characters, for example a word, a letter, a sentence, in javascript, it's represented between \"quotes\" for example \"hello\" is a string.",
          ],
          [
            "number",
            "A number, is represented without \"\", so for example 12 is a number but \"12\" would be a string.<br>This means that if you do 12+1 you get 13, but if you do \"12\"+\"1\", you get \"121\".",
          ],
          [
            "boolean",
            "A boolean is a value that is either true, or false.",
          ],
          [
            "array",
            "An array is a list of values, it's represented between []. For example [1,2,3] is an array.",
          ],
          [
            "object",
            "An object is a list of key-value pairs, it's represented between {}. For example { a: 1, b: 2, c: 3} is an object.<br>A database 'entry' is therefore some kind of object!",
          ],
        ]),
        "A complex object/entry, containing many values of all kinds of types could look like this:",
        "<pre>"+ $$.string.htmlify($$.json.pretty({
          name: "Alberto",
          birthday: "1489-04-09",
          age: 36,
          occupations: [ "eating", "resting", "working", "sleeping", ],
          details: {
            favoriteColor: "blueish green",
            likesMountains: true,
            mathInterests: [ "pi", 3.14, ],
          },
        }, true)) +"</pre>",
      ],
    },

    //
    //                              KEYS AND LABELS

    {
      word: "Key and Label",
      definition: [
        'A "key" is where a value is stored in an entry. In general it\'s better to use keys without spaces, containing a set of basic characters and numbers.',
        'An example of good key: <code>contactName</code>.',
        'The input "label" is the text displayed on the left of the input in the edition window. It\'s just a convenience so that edition windows are nicer to use.',
        'An example of label: <code>Contact Name</code>.',
        'If no label is specified, the key will be used as input label.'
      ],
    },

    //
    //                              KEYS AND DEEP KEYS

    {
      word: "Keys and deep keys",
      definition: [
        'In some places of dato, it\'s specified you can use a "deepkey". So you may have wondered what is a deep key.',
        'The "key" is like the "index" at which you can find a value in an object or array.',
        'The "deepkey" is the same thing, but saying that you can choose a deep key, means that you can choose also a value in a subobject.',
        '',
        'Maybe the easiest way to explain this is to give an example. In the entry given above:',
        '&nbsp;&nbsp;- to the key <code>name</code> corresponds the value <code>"Alberto"</code>,',
        '&nbsp;&nbsp;- to the key <code>age</code> corresponds the value <code>36</code>,',
        '&nbsp;&nbsp;- to the deep key <code>details.favoriteColor</code> corresponds the value <code>"blueish green"</code>',
        '&nbsp;&nbsp;- to the deep key <code>occupations[2]</code> corresponds the value <code>"working"</code>',
        '&nbsp;&nbsp;- to the deep key <code>details.mathInterests</code> corresponds the value <code>["pi",3.14]</code>',
        '&nbsp;&nbsp;- to the deep key <code>details.mathInterests[0]</code> corresponds the value <code>"pi"</code>',
        '',
        'As you can see in the above examples, the syntax is using <code>.</code> and <code>[]</code> as ways to specify children keys, just like in javascript syntax. This is why it\'s better to use keys with only basic characters and numbers, so that you don\'t have to learn all the nits and tricks of the syntax to know how to specify your deep keys.',
        '',
        'In dato not everywhere deep keys are supported. But everywhere it is specified you can use a deep key, a simple key is valid as well.'
      ],
    },

    //                              ¬
    //

  ];
};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  MAKE TABLE
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

function makeTable (lines) {
  var finalString = "<table>";
  _.each(lines, function (line) {
    finalString += "<tr>";
    _.each(line, function (content) {
      finalString += "<td>";
      finalString += content;
      finalString += "</td>";
    });
    finalString += "</tr>";
  });
  finalString += "</table>";
  return finalString;
};

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

module.exports = function () {
  uify.alert({
    title: "Glossary",
    contentClass: "documentation",
    content: function ($container) {
      _.each(wordsList(), function (wordObject) {
        $container.div({
          class: "section",
          text: wordObject.word,
        });
        $container.div({
          class: "description",
          htmlSanitized: _.isArray(wordObject.definition) ? wordObject.definition.join("<br>") : wordObject.definition,
        });
      });
    },
  });
};
