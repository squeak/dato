require("squeak/extension/url");
var $$log = $$.logger("dato");
var uify = require("uify");
var tablify = require("tablify");
var zeusMaker = require("zeus");
var hestia = require("hestia");
var autosyncMethods = require("./autosync");
var globalSettings = require("../utilities/globalSettings");
var datoButtons = require("./buttons");
var descartes = require("descartes");

// backup hestia grapes in json format before they are modified and their methods are filled by descartes
var hestiaGrapesStringifiedAndIndexedBeforeAnyProcessing = _.chain(_.pluck(hestia.grapes, "name"))
  .object(hestia.grapes)
  .mapObject(function (grape) { return JSON.stringify(grape); })
  .value()
;

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  ADD EDIT TYPE BUTTON
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

/**
  DESCRIPTION: add button to propose to edit the type of this entry
  ARGUMENTS: ( tablifyConfig )
  RETURN: <void>
*/
function addEditGrapeButton (tablifyConfig) {

  _.each(tablifyConfig.entryTypes, function (grape) {

    // DO NOT ADD EDIT TYPE BUTTON FOR SYSTEM TYPES
    if (grape.group === "_system") return

    // SUPPORT OPENING "BASIC" SYSTEM TYPES, TO CUSTOMIZE THEM IF DESIRED
    else if (grape.group === "_basics") {
      var eId = JSON.parse(hestiaGrapesStringifiedAndIndexedBeforeAnyProcessing[grape.name]);
      eId.type = "type";
      delete eId.group;
      var alertSystemType = "The type you just opened is a system type. If you edit and save it without renaming it, your modified version will replace the system default one.";
    }

    // NORMAL TYPE
    else eId = grape._id;


    // TOOLBAR BUTTON CONFIGURATION
    var editTypeButton = {
      name: "editType",
      icomoon: "sliders",
      title: "edit the type of this entry",
      click: function () {
        $$.url.hash.set({ db: "_types", e: [ eId ], });
        if (alertSystemType) uify.alert.warning(alertSystemType);
      },
    };

    // EDIT
    if (!grape.edit) grape.edit = {};
    if (!grape.edit.buttons) grape.edit.buttons = [];
    if (!_.findWhere(grape.edit.buttons, { name: "editType", })) grape.edit.buttons.push(editTypeButton);

    // VIEW
    if (!grape.view) grape.view = {};
    if (!grape.view.buttons) grape.view.buttons = [];
    if (!_.findWhere(grape.view.buttons, { name: "editType", })) grape.view.buttons.push(editTypeButton);

  });

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  CREATE TABLIFY
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

function createTablify (dbTablifyConfig, settings, options, callback) {

  //
  //                              DEFAULT CONFIG ELEMENTS

  // TABLIFY CONFIG OPTIONS CONSTANT IN DBISPLAY (mandatory ones overwrite what was gotten, optionals not)
  var dbisplayConfig = {
    mandatory: {
      name: settings.user +"--"+ options.name,
      $container: settings.$container,
      $toolbarContainer: settings.$container,
      $tutorialContainer: settings.$container.parent(),
      synchronizationTools: "statusOnly",
      class: "datobase--"+ options.name,
      pouchsInCache: descartes.config.pouchsCache,
      folderDisplayProcess: function (folderDisplayObject) {
        var folderDisplayObjectCloned = $$.clone(folderDisplayObject, 99);
        return descartes.autofillMethods(folderDisplayObjectCloned);
      },
      // modify edit button display in phonebar depending on if there is selected entries
      onEntrySelect: function () { window.datoActualizePhonebarButton("editSelected", this); },
      // make sure that global button has proper status, and is unclicked when the user request to open an entry
      onEntryOpen: function () { window.datoActualizePhonebarButton("windifiesDisplay", this); },
      onEntryEdit: function () { window.datoActualizePhonebarButton("windifiesDisplay", this); },
    },
    optional: {
      keyboardifyContext: options.name, // optional in order not to overwrite the one set in home and types db
      display: {
        _recursiveOption: true,
        title: options.name, // make sure db has a display.title set, otherwise it'll be showing username--options.name and that's not pretty
        fab: "none",
      },
      toolbarHelpCustomize: function (helpMenuButtons) {
        var tablifyApp = this;

        // blank
        helpMenuButtons.push({ type: "space", });
        // credits
        helpMenuButtons.push(datoButtons.credits());
        // issues page
        helpMenuButtons.push(datoButtons.issuesPage());

        return helpMenuButtons;

      },
    },
  };

  // IF AUTOSYNC IS ENABLED, PASS CREDENTIALS TO TABLIFY SO IT CAN LOGIN TO DATABASES AUTOMATICALLY
  if (settings.autosync.isEnabled()) {
    dbisplayConfig.mandatory.synchronizationCredentialsList = {};
    var decomposedUrl = $$.url.decompose(settings.autosync.url);
    var couchServerUrl = decomposedUrl.domain + (decomposedUrl.path + "").replace(/[^\/]+\/?$/, "").replace(/\/$/, ""); // NOTE: same as what was used in tablifyApp.getCouchServerId, if modify this, that one should be modified as well
    dbisplayConfig.mandatory.synchronizationCredentialsList[couchServerUrl] = {
      username: page.authenticatedUser.name,
      password: page.authenticatedUser.p,
    };
  };

  //
  //                              MAKE FULL CONFIG

  // add parameters passed in arguments
  var dbTablifyConfigFinal = $$.defaults(dbisplayConfig.optional, dbTablifyConfig, dbisplayConfig.mandatory);

  // add button to postify editors to propose to edit the type of this entry
  if (!options.onlyUseHardcodedConfig) addEditGrapeButton(dbTablifyConfigFinal);

  //
  //                              ADD CALLBACK TO TABLIFY

  var previousCallback = dbTablifyConfigFinal.callback;
  dbTablifyConfigFinal.callback = function (App) {

    // set original name of db in tablify app
    App.datobject = {
      tablifyApp: App,
      originalDatabaseName: options.name,
    };

    // existing callback function
    if (previousCallback) previousCallback.apply(this, arguments);

    // setup toolbar color when opening db
    window.datoActualizePhonebarButton("refreshColor", App);

    // all done
    callback(App.datobject);

  };

  //
  //                              BEFORE CREATING TABLIFY HOOK

  if (options.beforeCreatingTablify) options.beforeCreatingTablify(dbTablifyConfigFinal);


  //
  //                              MAKE TABLIFY APP

  var App = tablify(dbTablifyConfigFinal);

  //                              ¬
  //

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  THROW ERROR PAGE
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

function throwErrorPage (err, $container, options) {
  $container.div({
    class: "error_message",
    htmlSanitized: '¡¡ could not find configuration for database "'+ options.name +'" !!<br>If you didn\'t create it, you should do that first from the <a href="/">databases list page</a>.',
  });
  $$log.error("Could not find configuration for database '"+ options.name +"'", err);
};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  START APP
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

/**
  DESCRIPTION: start dato app
  ARGUMENTS: ({
    !$container: <yquerjObject>,
    !user: <string>,
    !optionsMaker: <
      !name: <string> « the name of the database you want to open »,
      ?config: <tablify·config> «
        the tablify configuration you want to autofill missing parts
        if you pass this and set onlyUseHardcodedConfig to true, zeus will not be used to fetch config from pouch, but just to autofill some parts of this config
      »,
      ?onlyUseHardcodedConfig <boolean> « if true, will not try to fetch config from pouch, just use the given one »,
      ?beforeCreatingTablify: <function(<tablify·config>):<tablify·config>> «
        an optional function called just before creating tablify
        lets you, for example, make some last minute customization of the config,
      »,
    >,
    !autosync: <{
      status: <"disabled"|"enabled">,
      calculated: <boolean>,
      isEnabled: <function(ø):<boolean> « check if autosync is enabled »,
    }>
  })
  RETURN: <void>
*/
function startApp (settings) {
  var options = settings.optionsMaker(settings.autosync);

  //——————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
  //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
  //——————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
  //                                                  SETUP GLOBAL SETTINGS FROM LOCAL STORAGE
  //——————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

  globalSettings.set();

  // initialize descartes.config username if it's not already done
  descartes.config.username = settings.user;

  //——————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
  //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
  //——————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
  //                                                  GET DATABASE CONFIG
  //——————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

  // INITIALIZE ZEUS
  var zeus = zeusMaker({
    presets: hestia,
    descartes: descartes,
    openMultipleArrowsDebugger: function (tablifyConfigsFound) {

      uify.menu({
        title: "There are multiple database display configurations for this database name.<br>What do you want to do?",
        type: "warning",
        buttons: [
          {
            title: "Open all the configurations to remove the unnecessary ones.",
            key: "enter",
            click: function () {
              globalSettings.visitHomeDatabase(function (homeTablifyApp) {
                _.each(tablifyConfigsFound, function (entry) {
                  homeTablifyApp.openEntryEditor(homeTablifyApp.Collection.get(entry._id));
                });
              });
            },
          },
          {
            title: "Do nothing for now, just use the first one randomly.",
            key: "esc",
          },
        ],
      });

    },
    openMultipleGrapesDebugger: function (typeName, postifyConfigsFound) {

      uify.confirm({
        type: "warning",
        content: [
          "There are multiple type configurations for '"+ typeName +"' type name. Only the first one will be used.",
          "",
          "It is recommended that you remove the unnecessary types.",
          "Do you want to open all the configurations to remove the unnecessary ones now?"
        ].join("<br>"),
        ok: function () {
          globalSettings.visitTypesDatabase(function (typesTablifyApp) {
            _.each(postifyConfigsFound, function (entry) {
              typesTablifyApp.openEntryEditor(typesTablifyApp.Collection.get(entry._id));
            });
          });
        },
      });

    },
  });

  //——————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
  //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
  //——————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
  //                                                  CREATE INTERFACE
  //——————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

  return new Promise(function(resolve, reject) {

    // AUTOFILL TABLIFY OPTIONS FROM GIVEN ONE
    if (options.config && options.onlyUseHardcodedConfig) zeus.autofillTablifyOptions(options.config).then(makeTable).catch(makeErrorPage)
    // OR FETCH TABLIFY OPTIONS FROM POUCH (if not found and config passed, create tablify with just passed config)
    else zeus.getTablifyOptions(options.name, options.config).then(makeTable).catch(function (err) {
      if (err.error == "arrow-not-found" && options.config) zeus.autofillTablifyOptions(options.config).then(makeTable).catch(makeErrorPage)
      else makeErrorPage(err);
    });

    function makeTable (dbTablifyConfig) {
      createTablify(dbTablifyConfig, settings, options, resolve);
    };

    function makeErrorPage (err) {
      throwErrorPage(err, settings.$container, options);
      reject(err);
    };

  });

  //——————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
  //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
  //——————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

/**
  DESCRIPTION: create a tablify table, fetching/autofilling entries and database configurations using zeus
  ARGUMENTS: (
    !$container <yquerjObject> « container in which to create tablify »,
    !optionsMaker <function(autosync<see startApp·autosync>):<see startApp·optionsMaker>>,
  )
  RETURN: <Promise.then(doneObject <{
    tablifyApp: <tablify·return> « the created tablify app »,
  }>)>
*/
module.exports = function ($container, optionsMaker) {

  // create a loading spinner but only if the app take too long to start (more than 500ms)
  var spinner = uify.spinner({
    $container: $container,
    text: "Loading dato app...",
    icon: "dato",
    // overlay: true,
    overlay: "url(\"/share/contributopia.jpg\")",
    delay: 500,
  });

  // GET CURRENT USERNAME
  var currentUser = page.authenticatedUser ? page.authenticatedUser.name : "_visitor_";

  // FETCH AUTOSYNC SETTINGS AND START APP
  return new Promise(function(resolve, reject) {

    autosyncMethods.getSettings(function (autosyncSettings) {
      startApp({
        $container: $container,
        user: currentUser,
        optionsMaker: optionsMaker,
        autosync: $$.methods(autosyncSettings || {}, autosyncMethods.methods),
      }).then(function (doneObject) {
        spinner.destroy();
        resolve(doneObject);
      }).catch(reject);
    });

  });

};
