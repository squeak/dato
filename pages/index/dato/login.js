var loginDialog = require("electrode/lib/login-dialog");
var publicConfig = require("../../../config/public");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

module.exports = function (status, callback, fallbackCallback) {

  loginDialog({

    // status
    status: status,

    // title and images
    titleText: {
      default: "Your session has timed out, please log in again.",
      mustlog: "Your session has timed out, please log in again.",
      error: "Internal error attempting to login to dato"+ (publicConfig.adminEmail ? "<br><small>if the problem persists, please write to "+ publicConfig.adminEmail +" to notify the issue</small>" : ""),
      autherror: "username or password invalid",
      unauthorized: "You don't seem to be allowed to access this page with this account"+ (publicConfig.adminEmail ? "<br><small>feel free to write to "+ publicConfig.adminEmail +" for support.</small>" : ""),
    },
    imageUrl: {
      default: "/share/dato.svg",
      unauthorized: "/pages/login/unauthorized.jpg",
      error: "/pages/login/error.gif",
      autherror: "/pages/login/autherror.gif",
      mustlog: "/share/dato.svg",
      loggedOut: "/share/dato.svg",
    },

    loginCallback: callback,

    // add a button so user can choose to stay offline and not be blocked in a login dialog loop when accessing, creating and deleting databases
    additionalButtons: {
      title: "stay offline",
      click: function (e) {
        this.destroy();
        if (fallbackCallback) fallbackCallback("Stay offline activated.");
      },
    },

  });

};
