
module.exports = {
  home: require("./home"),
  types: require("./types"),
  db: require("./db"),
};
