
module.exports = function (App) {
  //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
  //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

  var tutorialConfig = {

    // PRESENTATION TEXT
    presentationText: [
      "This tutorial will show you how to create entries types in dato.",
      "If you need there is a glossary in <span class=\"icon-question2\"></span> explaining a few concepts and words that may not be straightforward.",
      '<small>You can stop the tutorial any time clicking the <small><span class="icon-cross"></span></small>.</small>',
      '<small>To reopen it later, click on <span class="icon-question2"></span> in the toolbar and chose "how to create a type".</small>',
      "<big>Ready to start?</big>",
    ].join("<br>"),

    // STEPS
    steps: [
      //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
      //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
      //                                                  CREATE TYPE
      //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

      {
        title: "Create a type",
        text: [
          "To understand something of this tutorial, you should first understand what is an entry type.",
          "An entry type, is the set of settings that describe how some type of entry should be edited, viewed...",
          "For example, the 'contact' entry type, describes that when you edit a contact entry, it will display 'name', 'phone numbers', 'emails'... fields. It also describes that when you open a contact, you will see the list of phone numbers of this contact, and you can click on the number to call that person... All this kind of things.",
          "",
          "To create an entry type, use the <span class=\"icon-plus\"></span> button.",
          "Click on the + and when you're done, click 'next'."
        ].join("<br>"),
        nextCondition: function (App) { return $(".uify-dialog.uify-menu")[0]; },
        target: App.tutorialTargetToolbarButton("newEntry"),
        placement: App.tutorialPlacementLeftOr("top"),
      },

      {
        title: "What type of entry to create?",
        text: [
          "In the types database, you can create types and methods.",
          "Methods are custom functions that you can use when setting up some types (or databases, folders display).",
          "For now, let's create a simple type.",
          "Click 'type' and when you're done, click 'next'.",
        ].join("<br>"),
        target: function (App) {
          App.tutorialToolbarIsMenuHide();
          return $(".uify-dialog.uify-menu");
        },
        nextCondition: function (App) { return App.$el.find(".postify.active")[0]; },
        placement: "top",
        tippy: {
          appendTo: document.body,
          maxWidth: "none",
        },
      },

      {
        title: "Fill configuration for the type",
        text: [
          "Now you will need to fill the configuration for this entry type.",
          "You will at least have to chose a name to be able to find it and make use of it in your databases.",
        ].join("<br>"),
        target: function (App) {
          App.tutorialToolbarIsMenuHide();
          if ($$.isTouchDevice()) return App.$el.find(".postify.active > .titlebar")
          else return App.$el.find(".postify.active");
        },
        placement: App.tutorialPlacementLeftOr("bottom"),
        tippy: { arrow: $$.isTouchDevice() ? false : true, },
      },

      {
        title: "The list of inputs",
        text: [
          "When you edit an entry, there is a list of inputs. This is here that you can choose the list of inputs that will be shown for editing entries of this type.",
          "Open this EDIT section clicking in the black rectangle, to add new inputs.",
          "Then click the plus to add a new input, and then click on the new input to set it up.",
          "",
          "The first thing you need to set, is the 'key' attribute, this is where the value of this input will be stored in the entry.",
          "Then 'inputifyType' defines the type of input to use (there are many types of inputs: numbers, texts, dates, sliders, multichoices... the best way to understand what they do is to test them).",
          "Then you can customize other options as you wish.",
          "",
          "When you're done setting up this input, hit the <span class=\"icon-arrow-left3\"></span> button in top of this window, setup any other inputs you want, and when you're done, go back to the general list of configuration options with the same <span class=\"icon-arrow-left3\"></span> button.",
        ].join("<br>"),
        target: function (App) { return App.$el.find(".postify.active .inputify[inputify-name=edit]"); },
        placement: App.tutorialPlacementLeftOr("bottom"),
        tippy: {
          offset: [0, 30],
          maxWidth: App.toolbar.effectiveStyle == "menu" ? "" : "50vw",
        },
      },

      {
        title: "Configure the view window",
        text: [
          "When you double click on an entry, it opens it's view window.",
          "This section configures how will look the view window for this type of entries.",
          "Open this VIEW section, to add a list of fields to display in the view window.",
          "Add a new field with the plus, and click on it to set it up.",
          "",
          "You have now two options: 'key' or 'template'.",
          "&emsp;· If you chose 'key', you will have to chose which key you want to display, and how (for example, in a contact, you could choose the key 'phoneNumbers', this would show the list of phone numbers you have for this contact in the view window).",
          "&emsp;· If you chose 'template', you can create powerful custom templates to extract data from your entry and display it in the view window (hover over the 'template' label after choosing 'template' for more detailed information).",
          "",
          "When you're done setting up this field, hit the <span class=\"icon-arrow-left3\"></span> button in top of this window, setup any other fields you want to see in the view window, and when you're done, go back to the general list of configuration options with the same <span class=\"icon-arrow-left3\"></span> button.",
        ].join("<br>"),
        target: function (App) { return App.$el.find(".postify.active .inputify[inputify-name=view]"); },
        placement: App.tutorialPlacementLeftOr("bottom"),
        tippy: {
          offset: [0, 30],
          maxWidth: App.toolbar.effectiveStyle == "menu" ? "" : "50vw",
        },
      },

      {
        title: "Display configuration",
        text: [
          "You can now also choose how to make the 'title', 'subtitle', 'icon'... for entries of this type.",
          "Click on what you want to setup, and choose a method that will extract from the entry, the desired value to use as 'title', 'subtitle', 'icon'...",
        ].join("<br>"),
        target: function (App) { return App.$el.find(".postify.active .inputify_container[inputify-name=display]"); },
        placement: App.tutorialPlacementLeftOr("bottom"),
        tippy: {
          offset: [0, 30],
          maxWidth: App.toolbar.effectiveStyle == "menu" ? "" : "50vw",
        },
      },

      //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
      //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
      //                                                  SAVE TYPE AND USE IT IN A DB
      //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

      {
        title: "Save type and use it",
        text: [
          "Your type should now be ready.",
          "You can save it clicking on <span class=\"icon-floppy-disk\"></span>.",
          "Then, go back to the home database, select the database in which you want to be able to add this type of entries, edit it with <span class=\"icon-quill\"></span>, add this type to the list of 'availableTypes', save the database, open it, and add the entries!",
          "",
          "This tutorial is now finished, if you need visit the glossary and other tutorials in <span class=\"icon-question2\"></span>. If you have any difficulty, write to the administrator or developpers of dato if few or many things weren't clear.",
          "Hope you enjoy dato! ;)",
        ].join("<br>"),
        target: function (App) { return App.$el.find(".postify.active > .titlebar"); },
        placement: App.tutorialPlacementLeftOr("bottom"),
        tippy: { appendTo: document.body, },
      },

      //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
      //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
    ],

  };

  //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
  //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

  return tutorialConfig;

  //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
  //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
};
