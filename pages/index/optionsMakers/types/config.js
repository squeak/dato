var uify = require("uify");
var toolbarButtonsMaker = require("./toolbar");
var typeTutorial = require("./tutorial");
var datoButtons = require("../../dato/buttons");

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

module.exports = function () {
  return {
    //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
    //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

    //
    //                              GENERAL

    availableTypes: [ "type", "_method_", "_data_" ],
    pathsDatabase: false,
    keyboardifyContext: "_types",

    //
    //                              DISPLAY

    display: {
      _recursiveOption: true,

      title: "[types]",
      color: "#AD7B66",
      icon: "library",

      background: "/share/contributopia.jpg",
      backgroundCredit: function (background) {
        if (background == "/share/contributopia.jpg") return 'Special thanks to <a href="http://www.davidrevoy.com/">David Revoy</a> and <a href="https://contributopia.org/">contributopia</a> for this background image.<br>It is licensed under <a href="https://creativecommons.org/licenses/by/4.0/">Creative Commons 4.0 license</a>.'
      },

      grouping: "group",
      layouts: ["thinlist"],
      // currentLayout: "thinlist",

      filter: function (model) {
        return (
          model.get("type") == "type" ||
          model.get("type") == "_method_" ||
          model.get("type") == "_data_"
        );
      },

      rememberLimit: false,

    },

    //
    //                              ADD REMOTE TO MAKE SURE DB STAYS SYNC

    // remote: // let's make it simple: types db will not have any remote, it will count on home db for the synchronization to happen
    // NOTE: this means that if home db is not opened, types won't be synced, however the likeliness of home db not being opened (especially when working on types) is pretty low

    //
    //                              RAN WHEN TABLIFY HAS BEEN CREATED

    callback: function (tablifyApp) {

      // CHECK IF SHOULD PROPOSE TUTORIAL
      if (!tablifyApp.Collection.findWhere({ type: "type", })) uify.confirm({
        content: "It seems that you haven't created any types yet.<br>Do you want to display the 'how to create a type' tutorial?",
        overlayClickExits: false,
        ok: function () { tablifyApp.startTutorial(typeTutorial(tablifyApp)); },
      });

    },

    //
    //                              TOOLBAR ADDITIONAL BUTTONS

    toolbarButtons: toolbarButtonsMaker(),
    toolbarHelpCustomize: function (helpMenuButtons) {
      var tablifyApp = this;

      // glossary
      helpMenuButtons.unshift(datoButtons.glossary());

      // create type tutorial
      helpMenuButtons.unshift({
        icomoon: "profile5",
        title: "how to create a type",
        key: "t",
        click: function () {
          var App = this;
          App.startTutorial(typeTutorial(tablifyApp));
        },
      });

      // blank
      helpMenuButtons.push({ type: "space", });
      // credits
      helpMenuButtons.push(datoButtons.credits());
      // developpers documentation
      helpMenuButtons.push(datoButtons.developpersDoc());
      // issues page
      helpMenuButtons.push(datoButtons.issuesPage());

      return helpMenuButtons;

    },

    //                              ¬
    //

    //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
    //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  };
};
