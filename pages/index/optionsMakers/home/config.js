var uify = require("uify");
var toolbarButtonsMaker = require("./toolbar");
var datoTutorial = require("./tutorial");
var datoButtons = require("../../dato/buttons");
var autosyncMethods = require("../../dato/autosync");

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

module.exports = function (autosync) {
  return {
    //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
    //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

    //
    //                              GENERAL

    availableTypes: ["database", "link"], // the types that should be available for the user to create entries
    entryTypes: ["database", "link", "database-home"], // all the types that should be fetched to be editable in home db
    keyboardifyContext: "_home",

    //
    //                              DISPLAY

    display: {
      _recursiveOption: true,

      title: "[databases]",
      color: "#11F1E4",
      icon: "home",

      background: "/share/contributopia.jpg",
      backgroundCredit: function (background) {
        if (background == "/share/contributopia.jpg") return 'Special thanks to <a href="http://www.davidrevoy.com/">David Revoy</a> and <a href="https://contributopia.org/">contributopia</a> for this background image.<br>It is licensed under <a href="https://creativecommons.org/licenses/by/4.0/">Creative Commons 4.0 license</a>.'
      },

      grouping: "group",
      layouts: ["thumbnails"],
      // currentLayout: "thumbnails",

      filter: function (model) {
        return (
          model.get("type") !== "type" &&
          model.get("type") !== "_method_" &&
          model.get("type") !== "_data_" &&
          model.get("type") !== "database-home"
        );
      },

      rememberLimit: false,
      hiddenEntries: { hidden: true, },

    },

    remotes: function () {
      var tablifyApp = this;
      if (tablifyApp.config._id) return
      else return autosync.isEnabled() ? [{ location: "here", url: autosyncMethods.makeDatabaseUrl(autosync.url, "dato"), name: "dato", }] : [];
    },

    //
    //                              RAN WHEN TABLIFY HAS BEEN CREATED

    callback: function (tablifyApp) {

      // MAKE SURE LOCAL REMOTE DATABASES EXIST (IF AUTOSYNC IS ENABLED)
      autosync.createRemoteDatabaseIfNecessary(tablifyApp);

      // CHECK IF SHOULD PROPOSE TUTORIAL
      if (!tablifyApp.Collection.findWhere({ type: "database", })) uify.confirm({
        content: function ($container) {
          $container.img({ src: "/share/dato.svg", width: "15em", });
          $container.br();
          $container.br();
          $container.div({ text: "Hello :)", });
          $container.br();
          $container.div({ text: "It seems to be the first time you come here.", });
          $container.div({ text: "Do you want to display the tutorial to get started?", });
        },
        overlayClickExits: false,
        ok: function () { tablifyApp.startTutorial(datoTutorial(tablifyApp, autosync)); },
      });

    },

    //
    //                              TOOLBAR ADDITIONAL BUTTONS

    toolbarButtons: toolbarButtonsMaker(autosync),
    toolbarBatchRemoveDisable: true,
    toolbarHelpCustomize: function (helpMenuButtons) {
      var tablifyApp = this;

      // glossary
      helpMenuButtons.unshift(datoButtons.glossary());

      // create db tutorial
      helpMenuButtons.unshift({
        icomoon: "database2",
        title: "how to create a database and navigate in dato",
        key: "d",
        click: function () {
          var App = this;
          App.startTutorial(datoTutorial(tablifyApp, autosync));
        },
      });

      // blank
      helpMenuButtons.push({ type: "space", });
      // credits
      helpMenuButtons.push(datoButtons.credits());
      // developpers documentation
      helpMenuButtons.push(datoButtons.developpersDoc());
      // issues page
      helpMenuButtons.push(datoButtons.issuesPage());

      return helpMenuButtons;

    },

    //                              ¬
    //

    //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
    //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  };
};
