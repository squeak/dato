var autosyncMethods = require("../../../dato/autosync");
var dialogify = require("dialogify");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

function localRemotes (remotesList) {
  return _.where(remotesList, { location: "here" });
};

function removeLocalRemotes (remotesList) {
  var localRemotesList = localRemotes(remotesList);
  _.each(localRemotesList, function (remote) {
    autosyncMethods.removeDatabase(remote.name);
  });
};

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

module.exports = function (databaseGrapeType) {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  KEEP PREVIOUS beforeSave AND beforeRemove METHODS
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  if (!databaseGrapeType.edit) databaseGrapeType.edit = {};
  var previousBeforeSave = databaseGrapeType.edit.beforeSave || function (entryNow, entryBefore, callback) { callback(); };
  var previousBeforeRemove = databaseGrapeType.edit.beforeRemove || function (entryBefore, callback) { callback(); };

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  CUSTOM BEFORE SAVE METHOD
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  // check if local remotes have been removed and remove them if confirmed (if not confirmed, will prevent from saving)
  databaseGrapeType.edit.beforeSave = function (entryNow, entryBefore, callback) {
    var postified = this;

    // execute original before save
    previousBeforeSave.call(postified, entryNow, entryBefore, function () {

      // GET LIST OF REMOTES REMOVED ON SAVE
      var removedRemotes = _.reject(entryBefore.remotes, function (remote) {
        return _.findWhere(entryNow.remotes, { name: remote.name, });
      });
      var localRemotesToRemove = localRemotes(removedRemotes);

      // SOME REMOTE(S) DB(S) SHOULD BE REMOVED
      if (localRemotesToRemove.length) dialogify.input({
        type: "danger",
        input: { inputifyType: "normal", },
        comment: [
          "You have done some modifications to this database settings, which will induce the removal of the following remote databases:",
          "",
          "&emsp;"+ _.pluck(localRemotesToRemove, "name").join(", "),
          "⚠️ This action is not reversible! ⚠️",
          "",
          "To confirm the removal of these remote databases and all their documents, please enter the name of one of those remotes:",
        ].join("<br>"),
        ok: function (enteredText) {
          if (_.findWhere(localRemotesToRemove, { name: enteredText, })) {
            // remove remote databases
            removeLocalRemotes(localRemotesToRemove);
            // ok, proceed to entry removal
            if (callback) callback();
          };

        },
      })

      // NO REMOTE DB TO REMOVE
      else if (callback) callback();

    });

  };

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  CUSTOM REMOVE ENTRY WARNING
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  databaseGrapeType.edit.removeEntryWarningContent = function (entryBefore) {
    return [
      "Remove database '"+ entryBefore.name +"'?",
      "All entries you've stored in this database will be lost.",
      "⚠️ The following remotes will be removed as well: ⚠️",
      "<b>"+  _.pluck(localRemotes(entryBefore.remotes), "name").join(", ") +"</b>",
      "⚠️ This action is not reversible! ⚠️",
      "",
      "To confirm removing this database, please enter it's name here:",
    ].join("<br>");
  };

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  CUSTOM BEFORE REMOVE METHOD
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  // remove local remotes if confirmed (if not confirmed, will prevent from removing entry)
  databaseGrapeType.edit.beforeRemove = function (entryBefore, callback) {
    var postified = this;

    // execute original beforeRemove
    previousBeforeRemove.call(postified, entryBefore, function () {
      // remove remote databases
      removeLocalRemotes(entryBefore.remotes);
      // callback
      if (callback) callback();
    });

  };

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};
