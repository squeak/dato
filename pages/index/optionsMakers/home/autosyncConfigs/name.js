var autosyncMethods = require("../../../dato/autosync");

module.exports = function (databaseGrapeType, autosync) {

  //
  //                              GET NAME INPUTIFY CONFIG

  var nameInputifyConfig = _.findWhere(databaseGrapeType.edit.structure, { key: "name", });

  //
  //                              CUSTOMIZE NAME INPUT TO CREATE REMOTES ON CHANGE

  // if entry is new, update remote url on db name change (not for "database-home" type since it doesn't have name input)
  if (nameInputifyConfig) nameInputifyConfig.changed = function (value) {
    var inputified = this;
    if (value && inputified.storageRecursive("isNewEntry")) {
      var remotesInputified = inputified.getSibling("remotes");
      var remoteVal = remotesInputified.get() || [];
      remoteVal[0] = { location: "here", name: value, url: autosyncMethods.makeDatabaseUrl(autosync.url, value), };
      remotesInputified.set(remoteVal);
    };
  };

  //                              ¬
  //

};
