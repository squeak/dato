var autosyncConfigs = {
  // eventsTweak: require("./events"),
  editTweak: require("./edit"),
  nameTweak: require("./name"),
  remoteTweak: require("./remote"),
};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  CUSTOMIZE DATABASE GRAPES TO ENABLE AUTOSYNC
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

function customizeDatabaseGrapesToEnableAutosync (databaseTypeConfig, autosync) {

  var customizedDatabaseGrapeType = $$.clone(databaseTypeConfig, 99);

  // autosyncConfigs.eventsTweak(customizedDatabaseGrapeType);
  autosyncConfigs.editTweak(customizedDatabaseGrapeType);
  autosyncConfigs.nameTweak(customizedDatabaseGrapeType, autosync);
  autosyncConfigs.remoteTweak(customizedDatabaseGrapeType, autosync);

  return customizedDatabaseGrapeType;

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

/**
  DESCRIPTION:
    transform tablify config display to customize database and database-home types, to implement autosynchronization with remote db
    so that when a remote is added to a database, a remote db is created in couch
  ARGUMENTS: (
    !tablifyConfig <tablify·return>,
    !autosync,
  )
  RETURN: <void>
*/
module.exports = function (tablifyConfig, autosync) {

  _.each(tablifyConfig.entryTypes, function (entryTypeConfig, index) {

    // modify "database" and "database-home" entry types
    if (entryTypeConfig.name == "database" || entryTypeConfig.name == "database-home") {
      tablifyConfig.entryTypes[index] = customizeDatabaseGrapesToEnableAutosync(entryTypeConfig, autosync);
    };

  });

};
