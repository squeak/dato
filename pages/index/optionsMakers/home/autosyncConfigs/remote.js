var _ = require("underscore");
var $$ = require("squeak");
var descartes = require("descartes");
var autosyncMethods = require("../../../dato/autosync");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  INPUTS TO ADD
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

function additionalInputs (autosync) {
  return [

    //
    //                              NAME

    {
      key: "name",
      inputifyType: "normal",
      help: "Name to give to this remote in this server.",
      labelLayout: "intertitle",
      condition: 'zeus+inputify.condition.siblingValue("location", "here")',
      valueOutVerify: function (val) {

        if (!val) return
        else if (!val.match(/^[a-z]+[0-9a-z\-\_\(\)\+\$]*$/)) throw "invalid name for database"
        else if (val == "dato") throw "'dato' is a reserved database name, you cannot use it"
        else return val;

      },
      changed: function () {
        var inputified = this;

        // autofill url value on name change, if location is "here"
        var locationInputified = inputified.getSibling("location");
        if (locationInputified.get() == "here") {
          var urlInputified = inputified.getSibling("url");
          var remoteUrl = autosyncMethods.makeDatabaseUrl(autosync.url, inputified.get());
          urlInputified.set(remoteUrl);
        };

      },
    },

    //
    //                              LOCATION (here OR elsewhere)

    {
      key: "location",
      labelLayout: "hidden",
      help: [
        "To set autosynchronization with a database that should be created for you on this server, choose 'in this server' and just pass the name of the database.",
        "",
        "To set autosynchronization with a database that you don't own or that is on another server, choose 'in another server' and pass the full url of the database. (You will also need to setup CORS on that couchdb server to accept requests from this site.)",
      ],
      inputifyType: "radio",
      modified: 'zeus+inputify.modified.doOnSiblings("remake", ["name", "url", "filter", "usernameAndPasswordIntertitle", "username", "password"])',
      choices: [
        {
          _isChoice: true,
          label: "remote database in this server",
          value: "here",
        },
        {
          _isChoice: true,
          label: "remote database in another server",
          value: "elsewhere",
        },
      ],
    },

    //                              ¬
    //

  ];
};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

module.exports = function (databaseGrapeType, autosync) {

  //
  //                              GET REMOTE INPUTIFY CONFIG

  var remotesInputifyConfig = _.findWhere(databaseGrapeType.edit.structure, { key: "remotes", });
  var remoteInputStructure = remotesInputifyConfig.object.model.object.structure;

  //
  //                              CUSTOMIZE url AND filter INPUTS

  var urlInputifyConfig = _.findWhere(remoteInputStructure, { key: "url" });
  urlInputifyConfig.locked = 'zeus+inputify.condition.siblingValue("location", "here")';
  var filterInputifyConfig = _.findWhere(remoteInputStructure, { key: "filter" });
  filterInputifyConfig.condition = 'zeus+inputify.condition.siblingValue("location", { "$exists": true }, true)';
  var usernameAndPasswordIntertitleInputifyConfig = _.findWhere(remoteInputStructure, { key: "usernameAndPasswordIntertitle" });
  usernameAndPasswordIntertitleInputifyConfig.condition = 'zeus+inputify.condition.siblingValue("location", "elsewhere")';
  var usernameInputifyConfig = _.findWhere(remoteInputStructure, { key: "username" });
  usernameInputifyConfig.condition = 'zeus+inputify.condition.siblingValue("location", "elsewhere")';
  var passwordInputifyConfig = _.findWhere(remoteInputStructure, { key: "password" });
  passwordInputifyConfig.condition = 'zeus+inputify.condition.siblingValue("location", "elsewhere")';

  //
  //                              ADD ADDITIONAL INPUTS IN STRUCTURE

  _.each(additionalInputs(autosync), function (inputifyConfig) {
    remoteInputStructure.unshift(inputifyConfig);
  });

  //
  //                              MAKE SURE NAME AND URL ARE NOT KEPT WHEN NOT NECESSARY

  remotesInputifyConfig.object.model.valueOutProcess = function (remote) {
    if (!remote) return;
    if (remote.location == "elsewhere") {
      if (!remote.url) return;
      else if (remote.name) delete remote.name;
    };
    if (remote.location == "here") {
      if (!remote.name) return;
      if (remote.username) delete remote.username;
      if (remote.password) delete remote.password;
    };
    return remote;
  };

  //
  //                              CUSTOMIZE name INPUT IF database-home GRAPE

  if (databaseGrapeType.name == "database-home") {
    var nameInputifyConfig = _.findWhere(remoteInputStructure, { key: "name" });
    nameInputifyConfig.valueOutVerify = function (val) {
      if (!val) return
      else if (!val.match(/^[a-z]+[0-9a-z\-\_\(\)\+\$]*$/)) throw "invalid name for database"
      else return val;
    };
  };

  //
  //                              AUTOFILL METHODS WITH DESCARTES

  descartes.autofillMethods(remotesInputifyConfig);

  //                              ¬
  //

};
