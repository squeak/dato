require("squeak/extension/array");
var uify = require("uify");
var autosyncMethods = require("../../dato/autosync");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  FILTER ENTRIES TO GET IN DB
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

function filerEntriesToGetInDatabase (Collection, entryType, acceptableNames) {
  return  Collection.chain().filter(function (model) {
    // is type and listed in typesToExport
    return model.get("type") == entryType && _.indexOf(acceptableNames, model.get("name")) != -1;
  }).pluck("attributes").value(); // TODO: with this, if multiple entries have same name, all will be exported
};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  MAKE LIST OF DATABASES AND TYPES TO EXPORT
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

function makeListOfAllEntriesToExport (tablifyApp) {

  //
  //                              MAKE SURE ONLY DATABASES ARE EXPORTED

  var dbsToExport = _.pluck(tablifyApp.selected, "attributes");
  var selectedDatabases = _.where(dbsToExport, { type: "database"});
  if (!selectedDatabases.length) return uify.toast.error("No database selected.");
  if (selectedDatabases.length != tablifyApp.selected.length) alert("Will only export databases, "+ (tablifyApp.selected.length - selectedDatabases.length) +" other entries ignored.");

  //
  //                              GET LIST OF TYPES THAT SHOULD BE EXPORTED

  var typesToExport = [];
  _.each(selectedDatabases, function (dbConfig) {
    _.each(dbConfig.availableTypes, function (typeName) {
      typesToExport.push(typeName);
    });
  });
  typesToExport = _.uniq(typesToExport);

  // get list of type configs to export
  var typesConfigsToExport = filerEntriesToGetInDatabase(tablifyApp.Collection, "type", typesToExport);

  //
  //                              GET LIST OF METHODS THAT SHOULD BE EXPORTED
  // TODO: THIS STILL MISSES METHODS USED IN THE FOLDERS OF THIS DATABASE
  // TODO: THIS STILL MISSES DATA ENTRIES USED BY THIS DATABASE

  var methodsToExport = [];
  _.each($$.array.merge(selectedDatabases, typesConfigsToExport), function (entry) {
    JSON.stringify(entry).replace(/zeus\+[^\(]*\.customMethod\(\W*([\w\.]*)/g, function (match, methodName) {
      methodsToExport.push(methodName);
    });
  });
  methodsToExport = _.uniq(methodsToExport);

  var methodsConfigsToExport = filerEntriesToGetInDatabase(tablifyApp.Collection, "_method_", methodsToExport);

  //
  //                              CLONE ALL DBS AND NECESSARY TYPES (BEFORE THEY MAY BE MODIFIED IN NEXT STEP)

  var allEntriesToExportCloned = _.map(
    $$.array.merge(selectedDatabases, typesConfigsToExport, methodsConfigsToExport),
    function (entry) { return $$.clone(entry, 3); }
  );

  //
  //                              RETURN LISTS OF ENTRIES TO EXPORT

  return {
    databases: selectedDatabases,
    types: typesConfigsToExport,
    methods: methodsToExport,
    all: allEntriesToExportCloned, // in this, all have been cloned with 3 recursions
  };

  //                              ¬
  //

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

module.exports = function (tablifyApp, autosync) {

  // get lists of entries to export
  var listsOfEntriesToExport = makeListOfAllEntriesToExport(tablifyApp);
  var allEntriesToExport = listsOfEntriesToExport.all;

  // make file and dialog title
  var fileAndDialogTitle = _.pluck(listsOfEntriesToExport.databases, "name").join(", ") +" database(s)";
  if (listsOfEntriesToExport.types.length || listsOfEntriesToExport.methods.length) fileAndDialogTitle += " (";
  fileAndDialogTitle += _.compact([
    listsOfEntriesToExport.types.length ? "and "+ listsOfEntriesToExport.types.length  +" type(s)" : "",
    listsOfEntriesToExport.methods.length ? "and "+ listsOfEntriesToExport.methods.length  +" method(s)" : "",
  ]).join(" ");
  if (listsOfEntriesToExport.types.length || listsOfEntriesToExport.methods.length) fileAndDialogTitle += ")";
  var dialogTitle = "Export "+ fileAndDialogTitle +"?";

  // check what type of remotes the databases contain
  var allRemotesContainedInDatabases = _.chain(allEntriesToExport).where({ type: "database", }).pluck("remotes").flatten().compact().value();
  var hasLocalRemotes = _.findWhere(allRemotesContainedInDatabases, { location: "here", });

  // no remotes, just go ahead and propose to export
  if (!allRemotesContainedInDatabases.length) uify.confirm({
    content: dialogTitle,
    ok: function () { exportDatabasesAndTypes(); },
  })

  // choose if keeping remotes or not
  else uify.menu({
    title: dialogTitle,
    buttons: [
      {
        title: [
          "Export database(s) without remote synchronization (default)",
          "<small>do this if you want to share only the configuration, but not your data</small>",
        ].join("<br>"),
        class: "success",
        key: "enter",
        click: function () { exportDatabasesAndTypes(); },
      },
      {
        title: [
          "Export database(s) with remote synchronization",
          "<small>"+ (
            hasLocalRemotes ?
            "⚠️ use this to backup database(s), but not to share them" :
            "do this if you want the contents of the database(s) to be shared between all users"
          ) +"</small>",
        ].join("<br>"),
        class: "warning",
        click: function () { exportDatabasesAndTypes(true); },
      },
      {
        title: [
          "Export database(s) with remote synchronization setting up remotes urls",
          "<small>⚠️ use this to share your database(s) only if you want their contents to be shared between all users",
          "⚠️ you will also need to set rules on your remotes do make sure other users can access them</small>",
        ].join("<br>"),
        class: "warning",
        condition: hasLocalRemotes ? true : false,
        click: function () { exportDatabasesAndTypes("converted"); },
      },
      {
        title: "Cancel",
      },
    ],
  });

  /**
    DESCRIPTION: export all entries to file
    ARGUMENTS: (
      ?withRemotes: <true|"converted"> «
        if set will not remove remotes from databases,
        if "converted", will convert any local remote to a full url
      »
    )
    RETURN: <void>
  */
  function exportDatabasesAndTypes (withRemotes) {

    // remove remotes
    if (!withRemotes) _.each(allEntriesToExport, function (entry) {
      if (entry.type == "database") delete entry.remotes;
    })
    // or make sure remotes are full url
    else if (withRemotes == "converted") _.each(allEntriesToExport, function (entry) {
      if (entry.type == "database") _.each(entry.remotes, function (remoteObject) {
        if (remoteObject.location == "here") {
          remoteObject.location = "elsewhere";
          delete remoteObject.name;
        };
      });
    });

    // export entries to file
    tablifyApp.exportEntriesToFile({
      entries: allEntriesToExport,
      fileName: fileAndDialogTitle,
    });

  };

};
