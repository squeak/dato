require("squeak/extension/url");
var datoButtons = require("../../dato/buttons");
var exportDatabase = require("./toolbar_exportDatabase");
var uify = require("uify");
var autosyncMethods = require("../../dato/autosync");

//——————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//——————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————————

module.exports = function (autosync) {
  return [
    //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
    //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
    //                                                  SEPARATOR
    //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

    {
      type: "separator",
      position: "end",
      positionizeFirst: true,
    },

    //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
    //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
    //                                                  DATABASE ACTIONS
    //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

    datoButtons.dato([

      //
      //                              IMPORT

      {
        title: "import database(s) configuration(s) from file",
        icomoon: "folder-upload",
        key: "i",
        click: function () {
          var tablifyApp = this;
          tablifyApp.importEntriesFromFile(function (entriesToImport, validate) {
            var hasMethods = _.findWhere(entriesToImport, { type: "_method_", });
            uify.confirm({
              content: "Import "+ entriesToImport.length +" entries ?"+ (hasMethods ? "<br>The database(s) you are trying to import contains some custom methods, continue importing only if you trust who it comes from." : ""),
              type: hasMethods ? "danger" : undefined,
              ok: validate,
            });
          });
        },
      },

      //
      //                              EXPORT

      {
        title: "export selected database(s) configuration(s) to file",
        key: "e",
        icomoon: "folder-download",
        click: function () {
          var tablifyApp = this;
          exportDatabase(tablifyApp, autosync);
        },
      },

      //
      //                              SPACE

      { type: "space", },

      //
      //                              HOME DB SETTINGS

      {
        icomoon: "equalizer",
        title: "edit home database settings",
        key: "s",
        position: "bottom",
        positionizeFirst: true,
        click: function (e) {
          var tablifyApp = this;

          // get database-dato model in database
          var datoDatabaseModel = tablifyApp.Collection.findWhere({ type: "database-home", });

          // or if it doesn't exist yet, create it
          if (!datoDatabaseModel) datoDatabaseModel = tablifyApp.Collection.newModel({
            _id: $$.uuid(),
            name: "dato",
            type: "database-home",
            remotes: autosync.isEnabled() ? [{ location: "here", url: autosyncMethods.makeDatabaseUrl(autosync.url, "dato"), name: "dato", }] : [],
          });

          // open editor for database-dato model
          tablifyApp.openEntryEditor(datoDatabaseModel);

        },
      },

      //
      //                              RELOAD DB

      datoButtons.reloadDatabase(),

      //                              ¬
      //

    ]),

    //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
    //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
    //                                                  SPACES SWITCHER
    //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

    datoButtons.spacesSwitcher(),

    //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
    //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  ];
};
