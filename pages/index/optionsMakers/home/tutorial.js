
module.exports = function (App, autosync) {
  //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
  //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

  var tutorialConfig = {

    // PRESENTATION TEXT
    presentationText: [
      "This tutorial will show you how to get started with dato, how to create databases and navigate between opened databases.",
      '<small>You can stop it any time clicking the <small><span class="icon-cross"></span></small>.</small>',
      '<small>To reopen it later, click on <span class="icon-question2"></span> in the toolbar and chose "how to create a database and navigate in dato".</small>',
      "<big>Ready to start?</big>",
    ].join("<br>"),

    // STEPS
    steps: [],

  };

  //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
  //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  START
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  tutorialConfig.steps.push(
    //
    //                              LIST OF DATABASES

    {
      title: "Your list of databases",
      text: [
        "Here will be listed all the databases you create.",
        "To open one: double click on it.",
        "To edit one: select it and then click the <span class=\"icon-quill\"></span> (in the right toolbar).",
      ].join("<br>"),
      target: function (App) { return App.$footer; },
      placement: "bottom",
      tippy: {
        arrow: false,
        offset: [0, 200],
      },
    },

    //
    //                              CREATE A DB

    {
      title: "Create a database",
      text: [
        "Let's get started and create a database.",
        "Click on this <span class=\"icon-plus\"></span> button, and when you're done, click 'next'.",
      ].join("<br>"),
      nextCondition: function (App) { return $(".uify-dialog.uify-menu")[0]; },
      target: App.tutorialTargetToolbarButton("newEntry"),
      placement: App.tutorialPlacementLeftOr("top"),
      tippy: { appendTo: document.body, },
    },

    //
    //                              DB OR LINK

    {
      title: "What type of entry to create?",
      text: [
        "In the home database, you can create two types of entries: databases and links.",
        "A link is just an entry that when you open it, opens the url you've chosen.",
        "But for now, let's create a database.",
        "Click 'database' and when you're done, click 'next'.",
      ].join("<br>"),
      target: function (App) {
        App.tutorialToolbarIsMenuHide();
        return $(".uify-dialog.uify-menu");
      },
      nextCondition: function (App) { return App.$el.find(".postify.active")[0]; },
      placement: "top",
      tippy: {
        appendTo: document.body,
        maxWidth: "none",
      },
    },

    //
    //                              DB'S CONFIG

    {
      title: "Fill configuration for the database",
      text: [
        "Now you need to fill your database configuration.",
        "Fill whichever options you want, but at least choose a 'database name'",
        "",
        "In 'available types' choose 'note'. This means that in this database you will be able to add 'note' entries.",
        "(A 'note' is one of the entry type given by default, but to learn how to create your custom types, check out the 'how to create an entry type' tutorial.)",
      ].join("<br>"),
      target: function (App) {
        if ($$.isTouchDevice()) return App.$el.find(".postify.active > .titlebar")
        else return App.$el.find(".postify.active");
      },
      placement: App.tutorialPlacementLeftOr("bottom"),
      tippy: { arrow: $$.isTouchDevice() ? false : true, },
    }

    //                              ¬
    //
  );

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  REMOTES
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  if (autosync.isEnabled()) tutorialConfig.steps.push({
    title: "Setup some remotes",
    text: [
      "You've maybe noticed, that when you wrote the name of the database, the REMOTE input filled automatically with the same name.",
      "",
      "If you want to backup the entries you will create in this database, make sure some remotes are set here. This is also useful when you want to synchronize the database between different devices.",
      "",
      "Setting a remote means that your data will be saved on the database server you've chosen.",
      "",
      "If you write here a simple name, a remote database will be created on the server in which you use dato.",
      "However, you can also choose not to synchronize this database (hit the <span class=\"icon-cross\"></span> that will show when you hover over REMOTE), this will remove all set remotes.",
      "You may also create your own database server (or use one someone created), in that case create a database on that server, and setup it's full url here.",
    ].join("<br>"),
    target: function (App) { return App.$el.find(".postify.active .inputify[inputify-name=remotes]"); },
    placement: App.tutorialPlacementLeftOr("bottom"),
    tippy: { offset: [0, 30], },
  })
  else tutorialConfig.steps.push({
    title: "Setup some remotes",
    text: [
      "If you want to backup the entries you will create in this database, make sure some remotes are set here. This is also useful when you want to synchronize the database between different devices.",
      "",
      "Setting a remote means that your data will be saved on the database server you've chosen.",
      "",
      "To setup a remote database, you need first to create a database on a couchdb server, and then put the address of this db in this list.",
    ].join("<br>"),
    target: function (App) { return App.$el.find(".postify.active .inputify[inputify-name=remotes]"); },
    placement: App.tutorialPlacementLeftOr("bottom"),
    tippy: { offset: [0, 30], },
  });

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  FINISH TO CREATE THE DATABASE AND LEARN HOW TO NAVIGATE
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  tutorialConfig.steps.push(
    //
    //                              SAVE DB

    {
      title: "Save your new database",
      text: [
        "Ok, you should be done and ready to save the database.",
        "To do so, click on the <span class=\"icon-floppy-disk\"></span> in the top bar of this window",
      ].join("<br>"),
      target: function (App) { return App.$el.find(".postify.active > .titlebar"); },
      placement: App.tutorialPlacementLeftOr("bottom"),
    },

    //
    //                              OPEN IT

    {
      title: "Open the database",
      text: [
        "So now your database is created, and you can open it to populate it with some entries.",
        "To open it, just double click on it in the databases list.",
        "When you've done it, click next.",
      ].join("<br>"),
      target: function (App) { return App.$body.find(".tablify-entry"); },
      tippy: { appendTo: document.body, },
      placement: "bottom",
    },

    //
    //                              NAVIGATE IN DATO

    {
      title: "Navigate between opened databases",
      text: [
        "You're now in the database you've created, you can add entries with <span class=\"icon-plus\"></span> and start making use of the database.",
        "To get more information on how to use a database, you can (like in any database), click on <span class=\"icon-question2\"></span> in the top of the right toolbar and click 'how to use this app'.",
        "",
        "But for now, let's finish this tutorial.",
        "To go back to the home display, or to navigate between opened databases, you can click this <span class=\"icon-grid3\"></span> button"+ ($$.isTouchDevice() ? " (if you are on a mobile, you will have to open the toolbar by swiping left to see the button)" : "") +".",
        "Do it now and click on next when you're done.",
      ].join("<br>"),
      target: function (App) { return $(".space.current .spaces-switcher-button"); },
      nextCondition: function (App) { return $(".uify-spaces--switcher.visible")[0]; },
      tippy: { appendTo: document.body, },
    },

    //
    //                              NAVIGATE IN DATO

    {
      title: "Databases switcher",
      text: [
        "You're now in the databases switcher, and you can see here the list of databases you've currently opened.",
      ].join("<br>"),
      target: function (App) { return $(".uify-spaces--switcher .current_spaces > .space_thumbnail"); },
      tippy: { appendTo: document.body, },
      placement: "bottom",
    },

    //
    //                              NAVIGATE IN DATO

    {
      title: "Databases switcher",
      text: [
        "You can click the <span class=\"icon-home\"></span> button to open the homepage listing your databases.",
        "The second button <span class=\"icon-library\"></span> is to open the list of entry types.",
        "",
        "This tutorial is now over.",
        "If you want, you can open the <span class=\"icon-library\"></span>, click <span class=\"icon-question2\"></span> and start the 'how to create a type' tutorial.",
      ].join("<br>"),
      target: function (App) { return $(".uify-spaces--switcher .additional_buttons"); },
      tippy: { appendTo: document.body, },
      placement: "top",
    }

    //                              ¬
    //
  );

  //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
  //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

  return tutorialConfig;

  //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
  //||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
};
