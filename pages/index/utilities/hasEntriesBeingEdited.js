require("squeak/extension/url");

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

// check if a database has entries being edited
module.exports = function (datobject) {
  var thisSpaceHashObject = $$.url.hash.getAsObject(datobject.spaceObject.hash);
  return thisSpaceHashObject.e && thisSpaceHashObject.e.length ? true : false;
};
