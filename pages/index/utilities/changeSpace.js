require("squeak/extension/url");
require("squeak/extension/dom");
var uify = require("uify");
var keyboardify = require("keyboardify");
var makeDato = require("./makeDato");
var globalSettings = require("./globalSettings");
var makeThumbnail = require("./switcherThumbnails");
var hasEntriesBeingEdited = require("./hasEntriesBeingEdited");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  CHECK HASH CHANGE FOR ENTRIES TO EDIT AND VIEW
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

function checkHashChangeForEntriesToEditAndView (askedSpace, hashObjectBefore, hashObjectNow) {
  // IF ENTRIES IN HASH SET TO BE EDITED OR VIEWED (AND THEY WEREN'T THERE BEFORE) OPEN THEM
  if (askedSpace && askedSpace.tablifyApp) {
    askedSpace.tablifyApp.editEntriesFromHash(_.difference(hashObjectNow.e, hashObjectBefore.e));
    askedSpace.tablifyApp.viewEntriesFromHash(_.difference(hashObjectNow.v, hashObjectBefore.v));
  };
};

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

/**
  DESCRIPTION:
  ARGUMENTS: (
    spacified <uify.spaces·return>,
    askedDatabase <string>,
    callback <function(datobject<{
      originalDatabaseName: <string>,
      spaceUUID: <string>,
      tablifyApp: <tablify·return>,
      spaceObject: <uify.spaces·spaceObject>,
    }>)>,
    doNotVisitCreatedSpace <boolean>,
  )
  RETURN:
*/
function changeSpace (spacified, askedDatabase, callback, doNotVisitCreatedSpace) {
  if (!spacified.currentDatoSpace) spacified.currentDatoSpace = {};

  // MAKE SURE NO INPUT IS CURRENTLY FOCUSED BEFORE CHANGING SPACE (OTHERWISE CAN RESULT IN SPACES HALF MOVED)
  $$.dom.blur();

  // FIGURE OUT ASKED DATABASE
  if (!askedDatabase) {
    var hashString = $$.url.hash.getAsString();
    var hashObject = $$.url.hash.getAsObject(hashString);
    askedDatabase = hashObject.db || "_home";
  }
  else {
    var hashString = "db="+ askedDatabase;
    var hashObject = $$.url.hash.getAsObject(hashString);
  };

  // GET ASKED SPACE
  var askedSpace = spacified.get(askedDatabase);

  // CREATE SPACE IF IT DOESN'T ALREADY EXIST
  if (!askedSpace) {
    keyboardify.setContext(askedDatabase);
    var createdSpace = spacified.create({
      id: askedDatabase,
      contentMaker: function ($container) {

        // make container to contain tablify (so that toolbar is also inside $container, inside the space)
        var $tablifyContainer = $container.div();

        // make dato tablify
        makeDato($tablifyContainer, askedDatabase, spacified).then(function (datobject) {

          // update space name and thumbnail from tablify config
          var displayConfig = datobject.tablifyApp.config.display;
          createdSpace.name = displayConfig.title;
          createdSpace.options.name = displayConfig.title;
          createdSpace.options.thumbnail = makeThumbnail(datobject.tablifyApp);
          createdSpace.tablifyUUID = $$.uuid();
          spacified.switcher.actualize();

          // attach elements to each other in the best spaghetti way
          datobject.spaceUUID = createdSpace.tablifyUUID;
          datobject.spaceObject = createdSpace;
          createdSpace.tablifyApp = datobject.tablifyApp;
          createdSpace.datobject = datobject;

          // add this tablify to list of all tablifies
          globalSettings.allDatobases.push(datobject);

          // close this database
          datobject.close = function () {
            datobject.spaceObject.remove();
          };

          // attach some methods to datobject
          datobject.reloadDatabase = function (reloadCallback, doNotVisit) {
            if (hasEntriesBeingEdited(datobject)) uify.confirm({
              content: "You asked to close or reload database '"+ askedDatabase +"'.<br>Some entries are under edition. If you continue, unsaved changes to them will be lost.",
              type: "danger",
              ok: doReload,
            })
            else doReload();

            function doReload () {
              // save hash before reloading
              var currentHash = $$.url.hash.getAsString();
              // remove db space
              datobject.spaceObject.remove();
              // recreate db space from scratch
              changeSpace(spacified, askedDatabase, reloadCallback, doNotVisit);
              // reset hash to value saved before reloading
              $$.url.hash.set(currentHash);
            };

          };

          // if space not visited, now that db is created, reset keyboard to current space context
          if (doNotVisitCreatedSpace && spacified.currentDatoSpace) keyboardify.setContext(spacified.currentDatoSpace.id);

          // visit created db or not
          if (!doNotVisitCreatedSpace) createdSpace.visit();

          // eventual callback to execute
          if (callback) callback(datobject);

        }).catch(console.log.bind(console));

      },
    }, true);
    createdSpace.hash = hashString;

  }

  // IF DB NOT CHANGED JUST CHECK IF SOME ENTRY SHOULD BE OPENED/EDITED
  else if (askedSpace.id === spacified.currentDatoSpace.id) {
    spacified.currentDatoSpace.hash = hashString;
    if (callback) callback(spacified.currentDatoSpace.datobject);
  }

  // GO TO SPACE IF IT EXIST
  else {
    // BEFORE CHANGING SPACE, STORE HASH BEFORE
    var hashObjectBefore = $$.url.hash.getAsObject(askedSpace.hash);
    // VISIT SPACE
    if (!doNotVisitCreatedSpace) askedSpace.visit();
    // CHECK IF SOME ENTRY SHOULD BE OPENED/EDITED
    checkHashChangeForEntriesToEditAndView(askedSpace, hashObjectBefore, hashObject);
    // CALLBACK IF ASKED
    if (callback) callback(spacified.currentDatoSpace.datobject);
  };

};

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

module.exports = changeSpace;
