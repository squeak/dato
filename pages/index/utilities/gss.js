
// The gss (Global Sync Status) is a spinner displayed in any space when a sync is onging in any space
module.exports = function (spacified) {

  var $globalSyncStatusContainer = spacified.switcher.$switcher.div({ class: "dato-gss ", });
  var gss = {
    $container: $globalSyncStatusContainer,
    $spinner: $globalSyncStatusContainer.div({ class: "spinner icon-spinner", }),
    $infos: $globalSyncStatusContainer.div({ class: "infos", }),
    possibleStatuses: ["error", "warning", "active"],
    previousStatus: "none",
  };

  // check every two seconds global sync status
  gss.interval = setInterval(function () {

    // get sync statuses from all open databases
    var syncStatuses = [];
    gss.$infos.empty();
    _.each(spacified.spaces, function (space) {
      if (!space.tablifyApp) return; // just in case gss is kicking in too fast, before the db has finished initializing
      var syncStatus = space.tablifyApp.syncStatus();
      syncStatuses.push(syncStatus);
      gss.$infos.div({
        htmlSanitized: $$.getValue(space, "tablifyApp.config.display.title") +': <span class="sync_status--'+ syncStatus +'">' + syncStatus +'</span>',
      });
    });
    syncStatuses = _.compact(syncStatuses);

    // figure out global sync status
    var globalSyncStatus = "none";
    $$.breakableEach(gss.possibleStatuses, function (status) {
      if (_.indexOf(syncStatuses, status) !== -1) {
        globalSyncStatus = status;
        return "break";
      };
    });

    // set current sync status
    if (gss.previousStatus !== globalSyncStatus) {
      gss.$spinner.removeClass("sync_status--"+ gss.previousStatus);
      gss.$spinner.addClass("sync_status--"+ globalSyncStatus);
      gss.previousStatus = globalSyncStatus;
    };

  }, 2000);

};
