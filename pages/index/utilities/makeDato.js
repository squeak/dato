var dato = require("../dato");
var globalSettings = require("./globalSettings");
var optionsMakers = require("../optionsMakers");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  MAKE DATO
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

/**
  DESCRIPTION: make dato for the asked database
  ARGUMENTS: (
    !$container <yquerjObject>,
    !askedDatabase <string>,
    !spacified <uify.spaces·return>,
  )
  RETURN: <Promise.then(<{
    tablifyApp: <tablify·return>,
  }>)>
*/
module.exports = function ($container, askedDatabase, spacified) {

  // OPEN HOME
  if (askedDatabase == "_home") return dato($container, optionsMakers.home).then(function (datobject) {
    // attach allDatobases to home tablify app (used to reload databases when they're config is modified in home)
    datobject.tablifyApp.allDatobases = globalSettings.allDatobases;
    // pass datobject to next then
    return datobject;
  })
  // OPEN TYPES
  else if (askedDatabase == "_types") return dato($container, optionsMakers.types)
  // OPEN DB
  else return dato($container, _.partial(optionsMakers.db, askedDatabase));

};
