var _ = require("underscore");
var $$log = $$.logger("dato");
var descartes = require("descartes");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  CHECK FOR EVENTS
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

/**
  TYPE: <{ [cacheId]: <function[]> }>
*/
var eventsCache = {};

function checkForEvents (spacified) {

  // get home tablify app
  var homeTablifyApp = spacified.spaces[0].tablifyApp; // the 0 index is the home database, because home db space is always the first to be created

  // select only dbs with recurring events
  var dbsWithRecurringEvents = homeTablifyApp.Collection.filter(function (model) {
    return model.get("type") == "database" && model.get("recurringEvents");
  });

  // itterate through list of recurring events
  _.each(dbsWithRecurringEvents, function (model) {

    // get list of methods from cache if possible
    var dbCacheId = model.get("_id") +"---"+ model.get("_rev");
    if (eventsCache[dbCacheId]) var methods = eventsCache[dbCacheId]
    else {
      var methods = descartes.autofillMethods(_.clone(model.get("recurringEvents")));
      eventsCache[dbCacheId] = methods;
    };

    // execute each method
    _.each(methods, function (method, methodIndex) {
      if (_.isFunction(method)) method.call(spacified, model.attributes, methodIndex)
      else $$log.error("The recurring event method doesn't seem to be a function.", method, model.attributes);
    });

  });

};

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

/**
  DESCRIPTION: check for programmed recurring events to check in databases
  ARGUMENTS: ( spacified )
  RETURN: <void>
*/
module.exports = function (spacified) {

  var recurringEventsInterval;
  var initInterval = setInterval(function () {

    // start checking for events only once the first space has been created successfully
    if (spacified.spaces.length && spacified.spaces[0].tablifyApp) {
      clearInterval(initInterval);
      checkForEvents(spacified);
      recurringEventsInterval = setInterval(function () { checkForEvents(spacified) }, 40000);
    };

  }, 500);

};
