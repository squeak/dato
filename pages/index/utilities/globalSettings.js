var _ = require("underscore");
var $$ = require("squeak");
var uify = require("uify");
var dialogify = require("dialogify");
var tablify = require("tablify");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  LIST OF GLOBAL SETTINGS
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

var inputsStructure = [

  //
  //                              TABLIFY

  // the following is probably not useful anymore, since there is now a bottom bar in small screen devices
  // {
  //   name: "tablify",
  //   label: "TABLIFY",
  //   labelLayout: "intertitle",
  //   type: "object",
  //   object: {
  //     structure: [
  //
  //       {
  //         name: "display",
  //         labelLayout: "hidden",
  //         type: "object",
  //         object: {
  //           structure: [
  //             {
  //               inputifyType: "radio",
  //               name: "fab",
  //               label: "Show an additional big button to create/edit entries:",
  //               labelLayout: "stacked",
  //               choices: [
  //                 {
  //                   _isChoice: true,
  //                   label: "Never",
  //                   value: "none",
  //                 },
  //                 {
  //                   _isChoice: true,
  //                   label: "On touch devices",
  //                   value: "touchDevices",
  //                 },
  //                 {
  //                   _isChoice: true,
  //                   label: "Always",
  //                   value: "all",
  //                 },
  //               ],
  //             },
  //           ],
  //         },
  //       },
  //
  //     ],
  //   },
  // },

  //
  //                              INPUTIFY

  {
    name: "inputify",
    label: "INPUTIFY",
    labelLayout: "hidden",
    type: "object",
    object: {
      structure: [

        {
          inputifyType: "radio",
          name: "advancedInputDisplay",
          label: "By default, advanced inputs should be:",
          labelLayout: "stacked",
          choices: [
            {
              _isChoice: true,
              label: "Hidden",
              value: "hidden",
            },
            {
              _isChoice: true,
              label: "Displayed and marked with a *",
              value: "decoration",
            },
            {
              _isChoice: true,
              label: "Displayed like normal inputs",
              value: "normal",
            },
          ],
        },

      ],
    },
  },

  //                              ¬
  //

];

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  MODIFICATIONS CALLBACKS
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

// list of callbacks to execute when the globalSettings are set
var modificationCallbacks = {

  //
  //                              tablify

  tablify: function (settings, triggerRefresh) {
    if (settings.tablify) {

      // modify default tablify settings
      var fabValue = $$.getValue(settings, "tablify.display.fab");
      if (fabValue) $$.setValue(tablify.defaultConfig, "display.fab", fabValue);

      // refresh the app display
      if (triggerRefresh) _.each(globalSettings.allDatobases, function (datobject) {
        $$.setValue(datobject.tablifyApp.config, "display.fab", fabValue);
        datobject.tablifyApp.makeFab();
      });

    };
  },

  //
  //                              inputify

  inputify: function (settings, triggerRefresh) {
    if (settings.inputify) {

      // set settings in inputify defaults (so it's applied to every newly created inputify)
      window.inputify.extendDefaults(settings.inputify); // TODO: find better ways to do this, without inputify depending on window object

      // advancedInputDisplay refresh
      if (triggerRefresh) {
        if (settings.inputify.advancedInputDisplay == "hidden") $(".advanced_input").addClass("advanced_input-hidden")
        else if (settings.inputify.advancedInputDisplay == "normal") $(".advanced_input-hidden").removeClass("advanced_input-hidden")
        else if (settings.inputify.advancedInputDisplay == "decoration") $(".advanced_input").removeClass("advanced_input-hidden").addClass("advanced_input-decoration");
      };

    };
  },

  //                              ¬
  //

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

var globalSettings = {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  LIST OF ALL TABLIFIES
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  // allDatobases <datobject[]> « list of all tablifies currently displayed »,
  allDatobases: [],

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  EDIT GLOBAL SETTINGS
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  edit: function () {

    var currentGlobalSettings = globalSettings.get();

    dialogify.object({
      title: "Global settings",
      structure: inputsStructure,
      value: currentGlobalSettings,
      ok: function (value) {
        globalSettings.set(value, true);
      },
    });

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  GET/SET
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  get: function () {
    return $$.json.parseIfJson(localStorage.getItem("dato-global-settings")) || {};
  },

  /**
    DESCRIPTION:
      apply new modified globalSettings
      if newValue is defined, save it to localStorage
      else get settings from localStorage
      in all cases, apply them to the current UI
    ARGUMENTS: (
      ?newValue <object>,
      ?triggerRefresh <boolean> « true if should trigger refresh »,
    )
    RETURN: <object>
  */
  set: function (newValue, triggerRefresh) {

    // set settings in localStorage or get it from them
    if (newValue) localStorage.setItem("dato-global-settings", JSON.stringify(newValue))
    else newValue = globalSettings.get();

    // executed globalSettings modifications callbacks (to apply changes to the UI)
    _.each(modificationCallbacks, function (modifCallback) { modifCallback(newValue, triggerRefresh); });

    return newValue;

  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};

module.exports = globalSettings;
