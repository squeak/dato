
module.exports = function (tablifyApp) {

  // if image thumbnail, just return the url of the image
  var image = tablifyApp.$footer.find(".footer-image").attr("src");
  if (image) return image;

  // get icon charset from tablify footer
  var iconCharsCode = "";
  tablifyApp.$footer.find(".footer-icon").each(function(){
    iconCharsCode = window.getComputedStyle(this,':before').content;
  });

  // if no icon, just stop here
  if (!iconCharsCode) return;

  // create empty canvas
  var $canvas = $("<canvas>");
  var canvas = $canvas[0];
  var ctx = canvas.getContext("2d");
  canvas.width = 500;
  canvas.height = 500;

  // add icon in canvas
  ctx.fillStyle = $$.getValue(tablifyApp.config, "display.color") || "white";
  ctx.font = "500px icomoon";
  ctx.fillText(iconCharsCode.replace(/\"/g, ""), 0, 500);

  // set canvas content as favicon
  return canvas.toDataURL();
  // $("head .favicon").attr("href", canvas.toDataURL());

};
