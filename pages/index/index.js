var globalSettings = require("./utilities/globalSettings");
var hasEntriesBeingEdited = require("./utilities/hasEntriesBeingEdited");
var changeSpace = require("./utilities/changeSpace");
var gss = require("./utilities/gss");
var recurringEvents = require("./utilities/recurringEvents");
var toolbarify = require("toolbarify");
var phoneToolbar = require("./phonebar");
var windify = require("windify");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  CREATE SPACES
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

var spacified = require("./spacified");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

globalSettings.visitHomeDatabase = function (callback) {
  changeSpace(spacified, "_home", function (datobject) {
    if (callback) callback(datobject.tablifyApp);
  });
};

globalSettings.visitTypesDatabase = function (callback) {
  changeSpace(spacified, "_types", function (datobject) {
    if (callback) callback(datobject.tablifyApp);
  });
};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  STARTUP GENERAL UTILITIES
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

gss(spacified);
recurringEvents(spacified);

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  INITIALIZE
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

// THE IDEA HERE IS TO:
//   1. MAKE SURE home DB IS ALWAYS THE FIRST ONE CREATED
//   2. REOPEN DB (AND ENTRIES) AS EXPRESSED BY URL HASH

// CHECK IF SOME OTHER DB THAN HOME WAS ASKED
var hashObject = $$.url.hash.getAsObject();
if (hashObject && hashObject.db && hashObject.db !== "_home") {

  // store previous hash while creating home db
  var hashString = $$.url.hash.getAsString();
  $$.url.hash.set("");
  changeSpace(spacified, "_home", function () {

    // open database asked in hash
    $$.url.hash.set(hashString); // restore hash before creating space
    changeSpace(spacified);

  }, true);
}
// open home db by default, since no other db was asked
else changeSpace(spacified);

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  DISPLAY BOTTOM BAR FOR PHONES
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

var datoPhonebar = toolbarify({
  name: "datoPhonebar",
  $el: $app,
  position: "bottom",
  buttons: phoneToolbar(spacified, globalSettings).reverse(),
  size: 40,
  class: "dato-phonebar",
  style: "hidden",
  style_smallScreens: "condensed",
});

window.datoActualizePhonebarButton = function (buttonToActualize, tablifyApp) {

  var phonebarButtonToActualize = datoPhonebar.get(buttonToActualize);

  // refresh entry edit button
  if (buttonToActualize === "editSelected" && tablifyApp) {
    phonebarButtonToActualize.$icon.removeClass("icon-plus icon-quill icon-floppy-disk");
    if (tablifyApp.$el.find(".postify").length && !$app.hasClass("dato-hidden_windifies")) phonebarButtonToActualize.$icon.addClass("icon-floppy-disk")
    else if (tablifyApp.selected && tablifyApp.selected.length) phonebarButtonToActualize.$icon.addClass("icon-quill")
    else phonebarButtonToActualize.$icon.addClass("icon-plus");
  }

  // refresh windows display button
  else if (buttonToActualize === "windifiesDisplay") phonebarButtonToActualize.unclick()

  // refresh color
  else if (buttonToActualize === "refreshColor" && tablifyApp) {
    var contrastStyle = $$.color.contrastStyle(tablifyApp.currentDisplay.color);
    datoPhonebar.$toolbar.css(contrastStyle);
    datoPhonebar.$toolbar.find(".uify-button").css("color", "inherit");
  };

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  WATCH CHANGES IN HASH
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

$(window).on("hashchange", function (e) {
  changeSpace(spacified);
  // check if some entries are selected in db (in case of db change, so that phonebar button displays nicely)
  window.datoActualizePhonebarButton("editSelected", spacified.currentDatoSpace.tablifyApp);
  window.datoActualizePhonebarButton("refreshColor", spacified.currentDatoSpace.tablifyApp);
});

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  WATCH WINDOWS OPENING/CLOSING
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

windify.on("create", function (windified) {
  window.datoActualizePhonebarButton("editSelected", spacified.currentDatoSpace.tablifyApp);
});
windify.on("destroy", function (windified) {
  window.datoActualizePhonebarButton("editSelected", spacified.currentDatoSpace.tablifyApp);
});

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  ON BEFORE UNLOAD
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

window.onbeforeunload = function () {
  var result = _.reduce(globalSettings.allDatobases, function (memo, datobject) {
    return memo + hasEntriesBeingEdited(datobject);
  }, 0);
  if (result > 0) return true;
}

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
