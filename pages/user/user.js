require("squeak/extension/ajax");
require("squeak/extension/url");
var usersInputs = require("electrode/lib/users-inputs");
var uify = require("uify");
var dialogify = require("dialogify");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

var queryObject = $$.url.query.getAsObject();

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

// MAKE DIALOG
makeDialog();
function makeDialog () {
  dialogify.object({
    title: "Modify password for user "+ queryObject.u,
    structure: [
      usersInputs.password,
      usersInputs.passwordConfirmation,
    ],
    notClosable: true,
    ok: function (values) {
      return usersInputs.verifyPasswordsAndDo(values, function () {
        requestPasswordModification(values.password);
      });
    },
  });
};

function requestPasswordModification (newPassword) {

  $$.ajax.post({
    url: "/users/change-password/",
    data: {
      t: queryObject.token,
      p: newPassword,
    },
    success: function () {
      uify.alert({
        type: "success",
        content: "Successfully changed password, you will now be redirected to the homepage.",
        ok:     function () { $$.open("/"); },
        cancel: function () { $$.open("/"); },
      });
    },
    error: function (err) {
      var errorResponse = $$.json.parseIfJson(err.responseText) || {};
      uify.toast.error({
        message: errorResponse.error,
        duration: 10,
      });
      makeDialog();
    },
  });

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  BACKGROUND CREDITS
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

$app.div({
  class: "background_credits",
  htmlSanitized: 'Special thanks to <a href="http://www.davidrevoy.com/">David Revoy</a> and <a href="https://contributopia.org/">contributopia</a> for this background image.<br>It is licensed under <a href="https://creativecommons.org/licenses/by/4.0/">Creative Commons 4.0 license</a>.',
});

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
