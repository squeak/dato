
var $error = $app.div({
  class: "error",
})

$error.div({
  class: "error_message",
  text: "There's been an error or the page you asked doesn't exist",
});
$error.a({
  class: "error_redirect",
  href: "/",
  text: "return to homepage",
});

$app.div({
  class: "background_credits",
  html: 'Special thanks to <a href="http://www.davidrevoy.com/">David Revoy</a> and <a href="https://contributopia.org/">contributopia</a> for this background image.<br>It is licensed under <a href="https://creativecommons.org/licenses/by/4.0/">Creative Commons 4.0 license</a>.',
});
