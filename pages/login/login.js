var loginPage = require("electrode/lib/login-page");
var publicConfig = require("../../config/public");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  LOGIN PAGE OPTIONS
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

var loginPageOptions = {
  titleText: {
    default: "<big>login to access to dato</big>"+ (publicConfig.otherOptionsThanLogin ? "<br><small>(or see other options below)</small>" : ""),
    mustlog: "<big>please login to use dato</big>"+ (publicConfig.otherOptionsThanLogin ? "<br><small>(or see other options below)</small>" : ""),
    error: "Internal error attempting to login to dato"+ (publicConfig.adminEmail ? "<br><small>if the problem persists, please write to "+ publicConfig.adminEmail +" to notify the issue</small>" : ""),
    autherror: "username or password invalid",
    unauthorized: "You don't seem to be allowed to access this page with this account"+ (publicConfig.adminEmail ? "<br><small>feel free to write to "+ publicConfig.adminEmail +" for support, or check options below</small>" : ""),
  },
  imageUrl: {
    default: "/share/dato.svg",
    unauthorized: "/pages/login/unauthorized.jpg",
    error: "/pages/login/error.gif",
    autherror: "/pages/login/autherror.gif",
    mustlog: "/share/dato.svg",
    loggedOut: "/share/dato.svg",
  },
  background: "/share/contributopia.jpg",
  backgroundCredits: 'Special thanks to <a href="http://www.davidrevoy.com/">David Revoy</a> and <a href="https://contributopia.org/">contributopia</a> for this background image.<br>It is licensed under <a href="https://creativecommons.org/licenses/by/4.0/">Creative Commons 4.0 license</a>.',
};
if (publicConfig.otherOptionsThanLogin) loginPageOptions.comment = makeOtherOptions;

loginPage(loginPageOptions);

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  OTHER OPTIONS
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

function makeOtherOptions ($container) {

  var $otherOptionsToggler;
  var $otherOptions;

  var state = true;
  var text = "Other options";
  $otherOptionsToggler = $container.div({
    class: "other-options-toggler",
    html: "⯈ "+ text,
  }).click(function () {
    if (state) {
      $otherOptionsToggler.html("⯆ "+ text)
      $otherOptions.show()
    }
    else {
      $otherOptionsToggler.html("⯈ "+ text)
      $otherOptions.hide();
    };
    state = !state;
  });

  $otherOptions = $container.div({ htmlSanitized: publicConfig.otherOptionsThanLogin, }).hide();

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
