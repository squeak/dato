require("squeak/extension/string");
require("squeak/extension/url");
var $$log = $$.logger("dato");
var usersPage = require("electrode/lib/users-page");
var logoutSwitch = require("electrode/lib/logout-switch");
var uify = require("uify");
var passwordResetLinks = require("./passwordResetLinks");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  INITIALIZE
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

$$.ajax.get({
  url: "/users/get-db-url/",
  success: function (resp) {
    displayUsersPage($$.url.decompose(resp.url));
  },
  error: function (e) {
    $app.div({
      class: "error-message",
      text: "Couldn't fetch db url: "+ e.statusText,
    });
    $$log.error(e);
  },
});

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  DISPLAY USERS PAGE
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

function displayUsersPage (urlObject) {

  // FIGURE OUT SERVER URL PARTS
  if (urlObject) var serverUrl = $$.url.make(_.omit(urlObject, "username", "password"))
  else urlObject = {};

  // CREATE USERS PAGE
  usersPage({
    background: "/share/contributopia.jpg",
    backgroundCredits: 'Special thanks to <a href="http://www.davidrevoy.com/">David Revoy</a> and <a href="https://contributopia.org/">contributopia</a> for this background image.<br>It is licensed under <a href="https://creativecommons.org/licenses/by/4.0/">Creative Commons 4.0 license</a>.',
    serverUrl: serverUrl,
    adminUsername: urlObject.username,
    adminPassword: urlObject.password,
    additionalUserActions: [
      {
        icomoon: "chain-broken",
        title: "create password reset link",
        click: function () {
          var user = this;
          if (confirm("Request password reset link?")) passwordResetLinks.request(user);
        },
      },

    ],
  });

  // LIST PASSWORD RESET URLS
  uify.fab({
    $container: $app,
    icomoon: "chain-broken",
    title: "check list of currently active password reset links",
    position: { right: "stick", top: "default", },
    click: function () {

      uify.alert({
        title: "Currently active password reset links",
        content: function ($container) {
          passwordResetLinks.refreshList($container);
        },
      });

    },
  });

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
