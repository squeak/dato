var appConfig = require("../boot/config");

/**
  DESCRIPTION: list of pages that
  TYPE: <pageRouteObject[]>
  TYPES:
    pageRouteObject = {
      !url: <string> « absolute url »
      ?redirect: <string> « if defined, instead of serving a page, this url will redirect to the given value »,
      ?name: <string> « if not defined will be autofilled by electrode with the last chunk of the pageRouteObject.url »,
      ?title: <string> « if not defined will be set to pageRouteObject.name's value »,
      ?path: <string> «
        path to script and styles folder (relative to the pages directory)
        normally you'd want to leave this to it's default value that will be automatically set from pageRouteObject.url (pageRouteObject.url.replace(/^\//, ""))
      »,
      ?data: <string> «
        path to pages data (assets)
        normally you'd want to leave this to it's default value (appConfig.browserPath.pagesData + pageRouteObject.path)
      »,
      ?auth: <string[]> « list of users authorized to access this page »,
      // in addition to these keys, an additional "children" key will be created with a list of this page's children pages, to be passed to the page object in the browser
    }
*/
var pages = [
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  {
    url: "/",
    name: "index",
    path: "index/",
    title: "dato",
    auth: { roles: ["dato"], },
  },

  {
    url: "/error/",
    title: "dato error",
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
];

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

// ADD LOGIN PAGE ONLY IF authentication.couchUrl IS ENABLED
if (appConfig.authentication && appConfig.authentication.couchUrl) pages.push(
  {
    url: "/login/",
    title: "login to dato",
    noServiceWorker: true,
  },
  {
    url: "/users/",
    title: "dato users",
    noServiceWorker: true,
    auth: { roles: ["dato-admin"], },
  },
  {
    url: "/user/",
    title: "dato password change",
    noServiceWorker: true,
  }
);

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

module.exports = pages;
