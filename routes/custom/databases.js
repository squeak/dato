const _ = require("underscore");
const $$ = require("squeak/node");
let $$log = $$.logger("dato");
const fetch = require("node-fetch");
var appConfig = require("../../boot/config");
const couchAdminUrl = require("../../lib/makeRemoteCouchAdminUrl");
let autosynchronizationSettings = require("../../lib/autosynchronizationSettings");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  GET DB URL
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

/**
  DESCRIPTION: make db name with the following pattern "username--dbname"
  ARGUMENTS: ( req, res )
  RETURN: <void|string> « if void, will already have sent response »
*/
function getDatabaseUrl (req, res) {
  if (req.query.db) return req.user.name +"--"+ req.query.db
  else res.status(400).send({ error: "missing database name", });
};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  CREATE DATABASE AND ANSWER
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

/**
  DESCRIPTION: create a database in couch
  ARGUMENTS: (
    !req,
    !res,
    !dbToCreate <string>,
  )
  RETURN: <void>
*/
function createDatabaseAndAnswer (req, res, dbToCreate) {

  fetch(couchAdminUrl() + dbToCreate, { method: "PUT", })
    .then((fetchResponse) => fetchResponse.json())
    // REQUEST SUCCESS CREATED DATABASE AND SETUP PERMISSIONS OR ERROR
    .then((couchResponse) => {
      if (couchResponse.ok) {
        $$log.info("Successfully created database '"+ dbToCreate +"' in couch");
        setupDatabasePermissionsAndAnswer(req, res, dbToCreate);
      }
      else {
        if (couchResponse.error == "file_exists") return res.status(200).send({ ok: true, message: "exist-already" });
        else {
          $$log.error("Failed to create database '"+ dbToCreate +"' in couch, some unexpected error, here is couch response:", couchResponse);
          return res.status(400).send({ ok: false, error: "couch-error" });
        };
      };
    })
    // REQUEST ERROR
    .catch((err) => {
      $$log.error("Error trying to create database '"+ dbToCreate +"' in couch", err);
      return res.status(400).send({ ok: false, error: "request-error", });
    })
  ;

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  SETUP PERMISSIONS AND ANSWER
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

/**
  DESCRIPTION: set admin and member permissions to database
  ARGUMENTS: (
    !req,
    !res,
    !dbToCreate <string>,
  )
  RETURN: <void>
*/
function setupDatabasePermissionsAndAnswer (req, res, dbToCreate) {

  fetch(couchAdminUrl() + dbToCreate +"/_security", {
    method: "PUT",
    headers: { "content-type": "application/json", },
    body: JSON.stringify({
      admins:  { names: [ req.user.name ], roles: [] },
      members: { names: [ req.user.name ], roles: [] },
    }),
  })
    .then((fetchResponse) => fetchResponse.json())
    // REQUEST SUCCESS, PERMISSIONS SET OR NOT
    .then((couchResponse) => {
      if (couchResponse.ok) {
        $$log.info("Successfully setup '"+ dbToCreate +"' database permissions");
        return res.status(200).send({ ok: true, })
      }
      else {
        $$log.error("Failed to setup '"+ dbToCreate +"' database permissions", couchResponse);
        return res.status(400).send({ ok: false, error: "couch-error" });
      };
    })
    // REQUEST ERROR
    .catch((err) => {
      $$log.error("Error trying to change '"+ dbToCreate +"' database permissions", err);
      return res.status(400).send({ ok: false, error: "request-error", });
    })
  ;

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  REMOVE DATABASE AND ANSWER
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/


/**
  DESCRIPTION: remove a database in couch
  ARGUMENTS: (
    !req,
    !res,
    !dbToRemove <string>,
  )
  RETURN: <void>
*/
function removeDatabaseAndAnswer (req, res, dbToRemove) {

  fetch(couchAdminUrl() + dbToRemove, { method: "DELETE", })
    // PARSE COUCHDB RESPONSE
    .then((fetchResponse) => fetchResponse.json())
    // FIGURE OUT wHAT HAPPENED, IF WORKED/FAILED/UNNECESSARY
    .then((couchResponse) => {
      if (couchResponse.ok) {
        $$log.info("Successfully removed database '"+ dbToRemove +"' in couch");
        return res.status(200).send({ ok: true, })
      }
      else if (couchResponse.error == "not_found") {
        return res.status(200).send({ ok: true, status: "not_found", message: "Database doesn't exist.", });
      }
      else {
        $$log.error("Failed to remove database '"+ dbToRemove +"' in couch, maybe it doesn't exist", couchResponse);
        return res.status(400).send({ ok: false, error: "couch-error", });
      };
    })
    // ERROR SENDING DELETE REQUEST
    .catch((err) => {
      $$log.error("Request error trying to remove database '"+ dbToRemove +"' in couch", err);
      res.status(400).send({ ok: false, error: "request-error", });
    })
  ;

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  AUTOSYNCHRONIZATION ENABLED VERIFICATION
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

function checkAutosynchronizationIsEnabledAndRespondIfNot (req, res) {
  var autoSyncIsEnabled = couchAdminUrl() ? true : false;
  if (!autoSyncIsEnabled) res.status(400).send({ ok: false, error: "autosynchronization-disabled", });
  return autoSyncIsEnabled;
};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

let databasesRoutesList = [

  {
    url: "autosync-settings/",
    type: "GET",
    auth: { roles: ["dato"], },
    func: function (req, res, next) {
      res.status(200).send(autosynchronizationSettings);
    },
  },

];

if (appConfig.authentication && appConfig.authentication.couchUrl && appConfig.couchAdminAuthentication) databasesRoutesList.push(

  {
    url: "create/",
    type: "POST",
    auth: { roles: ["dato"], },
    func: function (req, res, next) {
      var dbToCreate = getDatabaseUrl(req, res);
      if (dbToCreate && checkAutosynchronizationIsEnabledAndRespondIfNot(req, res)) createDatabaseAndAnswer(req, res, dbToCreate);
    },
  },

  {
    url: "remove/",
    type: "POST",
    auth: { roles: ["dato"], },
    func: function (req, res, next) {
      var dbToRemove = getDatabaseUrl(req, res);
      if (dbToRemove && checkAutosynchronizationIsEnabledAndRespondIfNot(req, res)) removeDatabaseAndAnswer(req, res, dbToRemove);
    },
  }

);

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

module.exports = databasesRoutesList;
