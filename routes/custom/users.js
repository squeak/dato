const _ = require("underscore");
const $$ = require("squeak/node");
let $$log = $$.logger("dato");
const fetch = require("node-fetch");
var appConfig = require("../../boot/config");
const couchAdminUrl = require("../../lib/makeRemoteCouchAdminUrl");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  MODIFY PASSWORD FOR USER
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

function modifyUserPassword (res, username, password, successCallback) {

  getUserEntry(res, username, function (entry) {
    entry.password = password;
    postNewUserEntry(res, entry, function (couchResponse) {
      if (couchResponse.ok) {
        res.status(200).send({ ok: true, message: "Successfully modifier password." })
        if (successCallback) successCallback();
      }
      else res.status(400).send({ ok: false, error: couchResponse.error, });
    });
  });

};

function getUserEntry (res, username, callback) {

  fetch(couchAdminUrl() +"_users/org.couchdb.user:"+ username)
    .then((fetchResponse) => fetchResponse.json())
    .then(callback)
    .catch((err) => {
      $$log.error("Error trying to get entry for user '"+ username +"' in couch", err);
      return res.status(400).send({ ok: false, error: "Request error when trying to modify password.", });
    })
  ;

};

function postNewUserEntry (res, entry, callback) {

  fetch(couchAdminUrl() +"_users/"+ entry._id, {
    method: "PUT",
    headers: { "content-type": "application/json", },
    body: JSON.stringify(entry),
  })
    .then((fetchResponse) => fetchResponse.json())
    .then(callback) // successfully wrote entry
    .catch((err) => {
      $$log.errorColor("Error trying to set entry for user '"+ entry.name +"' in couch", err);
      return res.status(400).send({ ok: false, error: "Request error when trying to modify password.", });
    })
  ;

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  LIST OF CREATED PASSWORD CHANGES TOKEN
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

var currentPasswordChangeLinks = {};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  /users/... ROUTES
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

let usersRoutesList = [];
if (appConfig.authentication && appConfig.authentication.couchUrl) usersRoutesList = [

  {
    url: "get-db-url/",
    type: "GET",
    auth: { roles: ["dato-admin"], },
    func: function (req, res, next) {
      res.status(200).send({ url: couchAdminUrl(), });
    },
  },

  {
    url: "list-reset-password-links/",
    type: "GET",
    auth: { roles: ["dato-admin"], },
    func: function (req, res, next) {
      res.status(200).send(currentPasswordChangeLinks);
    },
  },

  {
    url: "reset-password-link/",
    type: "POST",
    auth: { roles: ["dato-admin"], },
    func: function (req, res, next) {
      if (!req.query.u) res.status(400).send({ ok: false, error: "missing user name", })
      else {
        var token = $$.random.password(60, "AZERTYUIOPMLJHGFDQWXCVBNazertyuiopmkjhfdsqwxcvbn1234567890");
        $$log.info("Generated password reset link for user '"+ req.query.u +"'.");
        currentPasswordChangeLinks[token] = req.query.u;
        res.status(200).send(token);
      };
    },
  },

  {
    url: "reset-password-link/",
    type: "DELETE",
    auth: { roles: ["dato-admin"], },
    func: function (req, res, next) {
      if (!req.query.token) res.status(400).send({ ok: false, error: "missing token", })
      else {
        delete currentPasswordChangeLinks[req.query.token];
        res.status(200).send({ ok: true, });
      };
    },
  },

  {
    url: "change-password/",
    type: "POST",
    func: function (req, res, next) {
      if (!req.body) res.status(400).send({ ok: false, error: "Missing post's data.", })
      else if (!req.body.p) res.status(400).send({ ok: false, error: "Missind new password you want to set for the user.", })
      else if (!currentPasswordChangeLinks[req.body.t]) res.status(400).send({ ok: false, error: "The token you provided doesn't seem valid or has expired.", })
      else modifyUserPassword(res, currentPasswordChangeLinks[req.body.t], req.body.p, function () {
        $$log.info("Password reset link for user '"+ currentPasswordChangeLinks[req.body.t] +"' has been used and removed.");
        delete currentPasswordChangeLinks[req.body.t];
      });
    },
  },

];

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

module.exports = usersRoutesList;
