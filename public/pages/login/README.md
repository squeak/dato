Credits to [wikipedia](https://wikipedia.org) for the `autherror.gif` and `unauthorized.jpg` images.

Other images have been produced for dato and are licensed under the same license as the package: AGPL-3.0-or-later.

