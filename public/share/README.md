
The `contributopia.jpg` image is licensed under the [Creative Commons 4.0 license](https://creativecommons.org/licenses/by/4.0/). No modifications have been made.
Thank you [David Revoy](http://www.davidrevoy.com/) and [contributopia](https://contributopia.org/) for your great graphics.

Other images and icons have been produced for dato and are licensed under the same license as the package: AGPL-3.0-or-later.
