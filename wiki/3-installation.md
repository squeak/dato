# Install

To make your own dato instance, you need to:
  - install dependencies,
  - install the app and start it,
  - choose your configuration (optional),
  - setup your ports opening, DNS, autostarting of the app.

## Prerequisites

- You will need to have [nodejs](https://nodejs.org/) installed,
- if you want to make the app private, you will need [a couchdb server](https://couchdb.apache.org/),
- and if you want to enable autosynchronization, this couchdb server will need to be reachable from the internet (unless you're just running dato locally).

## Install and start

You can install and start dato doing:
```bash
git clone git@framagit.org:squeak/dato.git
cd dato
npm install # this will take a few seconds/minutes to install all necessary dependencies
npm start # dato is ready when it shows the big colorful "dato electrode server listening on port XXXX"
```

At this point, you can already visit your browser at `http://localhost:9559` and test the app.

## Configure

If you want to enable authentication or auto-synchronization of databases, you will need to create (if it doesn't exist already) the file `config/private.js`. It's content should look like this:
(If you want authentication and/or autosynchronization of databases, and don't have a running couchdb instance, you will need to set one up. Check out [couchdb documentation](https://couchdb.apache.org/) for installation instructions.)
```js
module.exports = {
  authentication: {
    couchUrl: "http://localhost:5984/", // this assumes you run couchdb locally on port 5984
  },
  couchAdminAuthentication: {
    // couch admin user:
    username: "admin",
    // couch admin user's password:
    password: "adminPasswørd",
    // the url where your couchdb server is accessible from the internet:
    customCouchUrl: "https://myCouchDomain.tld/",
  },
};
```
When you've turned authentication on, to create dato users and give them the "dato" role. Visiting the "/users/" page (once you've started the app ;)) gives you a graphical interface for doing that.
For more details, check the [possible setups page](?setups).

Whenever you modify the configuration, you need to restart your app. Simply kill it's process hitting `ctrl + c` and rerun `npm start`.

The `config` folder is never modified on dato updates, so it's a good place to put your local configurations.
To know how to customize the `config/private.js` file, check `boot/defaultConfig.js` to see all the options you may customize. Options you choose in `config/private.js` will be added to the default configuration to make the running config, so just write what you want to modify.
You can use `config/private.js` to customize, for example, the port on which your app should run, production and debugging statuses...

### Customize login page

You can improve the display of the login page by creating (if it doesn't already exist) the file `config/public.js` and set some custom options.
The following options are available:
```js
module.exports = {

  // address to request for accounts or if there is issues with dato (showed in login page)
  adminEmail: "datoadmin@yourserver.tld",

  // some other options proposed to the user if they can't login (showed in login page, below login form, can be long because it's shown only if the user clicks on "other option")
  otherOptionsThanLogin: "Send a pigeon request to my house and I'll create an account for you :)<br>My address is:<br>9559 avenue Poseidon<br>Atlantis",

};
```

## Setup ports opening, DNS, autostarting the app...

Once the app is running, you need to open port 9559 (or the one you've chosen in `config/private.js`),
and configure your DNS to point to your app.
If you want dato to start automatically when your system starts, check out [pm2](https://pm2.io/), it's a good tool to achieve that.
