# Why dato?

Dato is an application that make it easy to generate and modify any type of database entries with a nice UI/UX.

With it, you can create and handle any amount of databases, choose and setup what type of entries to put in them and customize yourself the UI that will let you edit databases entries.

## "Philosophy"

You can think of dato as an app that can replace: address book apps, todo list apps, table sheets to store lists of things... because it's flexible enough to do all these the way you want it to be done.

The purpose of dato is not to propose a UI specific to some purpose, but something as customizable as possible, so that you can in a few minutes setup any kind of database. However, if you want a more specialized interface to improve the UX for your needs, you can create plugins to extend the default general functionalities.

## A webapp, and more?

Dato is at the moment running as a webapp and you can install it on any server or computer, you can also use it in mobiles and offline.

## Use it (or just test the public demo)

A public running instance of dato is available [here](https://publicdato.eauchat.org/).
This is a full featured version of the app in it's [public setup](?setups). Feel free to use it, any data you put on it is only kept locally in your browser, unless you choose yourself to synchronize it with some remotes.
