# Extensions

## Bookmarklet

By default, dato proposes a bookmark type which makes it easy to use a database to store bookmarks.  
For commodity, you may want to use a bookmarklet in your browser to interact with dato in order to add bookmark entries in a given database. To do so, you should first create your bookmarks database, and then add a bookmark in your browser with the following content:
```
javascript:(function(){

  /* make entry to add to db */
  var entry = {
    type: "bookmark",
    url: location.href,
    name: document.title || url,
    description: document.getSelection().toString(),
  };
  if (entry.description.length > 4000) {
    entry.description = entry.description.substr(0,4000) + '...';
    alert("The selected text is too long, it will be truncated.");
  };

  /* open dato to add entry */
  window.open(
    'https://{{SERVER_URL}}/#'+
    'db={{DB_NAME}}'+
    '&e=['+ encodeURIComponent(JSON.stringify(entry)) +']',
    '_blank'
  );

})();
```
Don't forget to replace {{SERVER_URL}} and {{DB_NAME}} with your server's address and bookmarks db name.  
For example:
```
javascript:(function(){

  /* make entry to add to db */
  var entry = {
    type: "bookmark",
    url: location.href,
    name: document.title || url,
    description: document.getSelection().toString(),
  };
  if (entry.description.length > 4000) {
    entry.description = entry.description.substr(0,4000) + '...';
    alert("The selected text is too long, it will be truncated.");
  };

  /* open dato to add entry */
  window.open(
    'https://publicdato.eauchat.org/#'+
    'db=mybookmarksdb'+
    '&e=['+ encodeURIComponent(JSON.stringify(entry)) +']',
    '_blank'
  );

})();
```

If you prefer the window to be a popup, you may also like to use the following instead:
```
javascript:(function(){

  /* make entry to add to db */
  var entry = {
    type: "bookmark",
    url: location.href,
    name: document.title || url,
    description: document.getSelection().toString(),
  };
  if (entry.description.length > 4000) {
    entry.description = entry.description.substr(0,4000) + '...';
    alert("The selected text is too long, it will be truncated.");
  };

  /* open dato to add entry */
  window.open(
    'https://{{SERVER_URL}}/#'+
    'db={{DB_NAME}}'+
    '&e=['+ encodeURIComponent(JSON.stringify(entry)) +']',
    '_blank','menubar=no,toolbar=no,scrollbars=yes,status=no,dialog=1'
  );

})();
```
