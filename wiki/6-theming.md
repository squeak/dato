# Theming

To create your own theme for dato, just create (if it doesn't exist already) a file named `theme.less` in `global` directory.
You can put any css/less code in this file to customize the display of the app.

You can also use it to customize color variables.
Here is the list of available variables and their default value, replace any one of them by what you want to customize the look of your dato.
```less
@base: black;
@baseTransparent: #00000088;
@baseTransparentMore: #00000033;
@baseTransparentLess: #000000CC;
@baseContrast: white;
@baseContrastTransparent: #FFFFFF88;

@primary: #FFFF17;
@primaryTransparent: #FFFF1788;
@primaryTransparentMore: #FFFF1733;
@primaryTransparentLess: #FFFF17CC;
@primaryContrast: black;

@secondary: #FFD703;
@secondaryTransparent: #FFD70388;
@secondaryTransparentMore: #FFD70333;
@secondaryTransparentLess: #FFD703CC;
@secondaryContrast: black;

@hover: #FF5700;
@hoverContrast: white;

@selected: #FFA217;
@selectedTransparent: #FFA21788;
@selectedTransparentMore: #FFA21733;
@selectedTransparentLess: #FFA217CC;
@selectedContrast: white;

@success: #6FD100;
@successContrast: black;
@error: #F00;
@errorContrast: white;
@warning: orange;
@warningContrast: black;

@basefontLight: Lato-Light;
@basefont: Lato;
@basefontBold: Lato-Bold;
```
