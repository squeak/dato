# Demos, tutorials, resources

![](https://squeak.eauchat.org/share/assets/dato/screenshot.png)

## In app tutorial

You can test dato from it's public instance [here](https://publicdato.eauchat.org/).
To learn how to use it, there is an in-app tutorial.

## Videos

There are also some tour, demos and tutorials available on the dato peertube channel: [datotutos@video.eauchat.org](https://video.eauchat.org/c/datotutos).

Tutorial videos:
  - [Dato tutorials - 1 - Creating a database](https://video.eauchat.org/w/eScsfUs1bbNSWZWqyRm7db)
  - [Dato tutorials - 2 - Refining db display and functionalities](https://video.eauchat.org/w/00b0c651-bd3d-4e3b-bb4a-f8dbec401583)

Demos and tours:
  - [Dato demo](https://video.eauchat.org/w/2dKr64CqGPy1j41Eif7jXd)

## Example configs

Some example configurations are also available in [the dato repository](https://framagit.org/squeak/dato/-/tree/master/examples). To test them, you can import them to your home database in dato using "database actions" > "import database(s) configuration(s) from file".
