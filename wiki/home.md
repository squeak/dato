
|databases made easy|
|:--:|

Store and organize anything.
Regain control on your data, choose where you store it, back it up.
Create databases, entries, with a nice UI/UX.

||v||
|--:|:--:|:--|
|>|[![¡try dato!](/share/assets/dato/try-dato.png)](https://publicdato.eauchat.org/)|<|
||^||

*Dato's goals:*
  - make it accessible for anyone to create any type of databases, put some contents in them, and synchronize them seamlessly,
  - helping users to keep control on their data,
  - empowering users in handling their data, and customizing their interaction with it,
  - making organizational freaks life more fun.


*Who is dato made for:*
  - anyone with interest in keeping control on the data they produce, and choose with who they share it,
  - non programmers who want advanced customization power,
  - programmer who believe UI can improve life sometimes ;)
  - you if you want your own customized app but don't have the time/money/ability to make it,
  - anyone who'd like to have an address book that supports nested folders,
  - obsessionally organized freaks,
  - me,
  - you?

|[(; give your own touch to your data ;)](https://publicdato.eauchat.org/)|
|:--:|
