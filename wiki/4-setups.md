# Possible setups

## Public

The public setup, allows anyone to use the app. The databases are stored by each user in their browser's memory. No auto-synchronization is setup, and if the users want it's databases to be synchronized with remote ones, they need to configure each database manually: create and setup the remote databases.

This is the default setup. If you don't customize `config/private.js`.

## Private with or without auto-synchronization of the databases with this server

In the private setup, the usage of the app is restricted to logged in users. Optionally, if the administrator sets it, each user's database can be automatically set to synchronize with according remote databases in the chosen server.

### To enable private mode:

You need to add the following configuration in `config/private.js` (create the file if necessary):
```js
authentication: {
  couchUrl: "http://localhost:5984/", // this assumes you run couchdb locally on port 5984, change it if necessary
}
```

If you want to use this mode, you will have to setup [a couchdb instance](https://couchdb.apache.org/).
You will also need to make sure that your couchdb instance has CORS enabled (you can do it from the "config" panel in the Fauxton web interface). You should set CORS to allow the domain where your dato app is served.

### To enable auto-synchronization of the users databases:

In addition with the previous point, you will also need to setup appropriately `couchAdminAuthentication` (also in `config/private.js`), it should look like this:
```js
couchAdminAuthentication: {
  // couch admin user:
  username: "admin",
  // couch admin user's password:
  password: "adminPasswørd",
  // the url where your couchdb server is accessible from the internet:
  customCouchUrl: "https://myCouchDomain.tld/",
}
```

### Configure couchdb so that it accepts requests from dato:

For couchdb to accept requests from dato, you should make some modifications to it's configuration.
To do so, create a file with the name you wish in `/opt/couchdb/etc/local.d/`.
For example: `sudo vim /opt/couchdb/etc/local.d/dato-added-settings.ini`.
And then add the following lines (adapting the "origins = ..." line with the url at which you installed dato):

```ini
[httpd]
enable_cors = true

[cors]
credentials = true
headers	= accept, authorization, content-type, origin, referer
methods	= GET, PUT, POST, HEAD, DELETE
origins = https://datoDomain.tld,https://otherDomain.tld

[couchdb]
users_db_security_editable = true
```

Then restart couchdb with `sudo service couchdb restart`.

Then in fauxton (couchdb graphical interface) you can open the "_users" database, and add "dato" and "dato-admin" to the roles that are members of this database.

You can also do this with curl with something like this (this code depends on the `jq` package):
```
couchUrl="https://admin:password@myCouchDomain.tld"
usersSecDoc=$(curl -X GET "$couchUrl/_users/_security")
usersSecDocModified=$(echo $usersSecDoc | jq '.members.roles += ["dato", "dato-admin"]')
curl -X PUT "$couchUrl/_users/_security" \
   -H "Accept: application/json" \
   -H "Content-Type: application/json" \
   -d "$usersSecDocModified"
```

### Add users, and grant them access to dato:

Once you've completed the previous steps, you should be able to start your dato app.

To add users, you will need to setup at least one administration account, to setup an admin account, you will need to manually create a user in your authentication couch db.
You can do this with curl with the following command:
```bash
curl -X PUT https://<myCouchDomain.tld>/_users/org.couchdb.user:<newAdminUsername> \
     -H "Accept: application/json" \
     -H "Content-Type: application/json" \
     -d '{"name": "<newAdminUsername>", "password": "<password to use>", "roles": ["dato-admin"], "type": "user"}'
```
you should replace in this command all the values between <> by the appropriate values in your situation.
The couch url is the one you've used in `couchAdminAuthentication.customCouchUrl` (or in `authentication.couchUrl` if it's a public one). The password will be used to login to the `/users/` page, but you can also reset it later.

You should now be able to login to the `/users/` page, and add new users. You should also make sure to set the role `dato` for users that should have access to the app.
Once you've created a user, you can also create a password reset link and send it to the person you created an account for. This link will be removed after they've used it to change their account password.

You may also create accounts using curl as documented [in the couchdb documentation](https://docs.couchdb.org/en/2.2.0/intro/security.html#creating-a-new-user), similarly to the way you created the first admin account, just don't forget to give them the role: `dato`.
