
module.exports = {
  title: "Screenshots",
  type: "images",
  assetsFolderName: "dato_screenshots",
  // style: "swipe", // not used because nicer with scroll (since swipe doesn't have titles)
};
