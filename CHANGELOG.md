# Version history

>  *Only major changes are listed here, there are always minor improvements as well.
> Improvements made to some dependencies are also listed, when those are developed alongside dato.*

## Version 1.7.5
- Improved "suggest", "select", "simple" and "multi" inputs display in phone
- Fixed bug with tickbox input which messed up the ordering of items when reopening entries, and improved display of ordering

## Version 1.7.4
- Fixed bug with inputify.modified.remake method still referenced, replaced it with doOnSiblings one

## Version 1.7.3
- Method sorting.date now supports passing the key where date is to be found in entry
  ⚠ This is a breaking change as dateFormat should now be passed as second argument, make sure to make the modification for your sortings to keep working
- Fixed sorting messed up when some entries have invalid dates
- Fixed toasts positioning
- Fixed many bugs with keyboard shortcuts, maybe not yet perfect, but at least it's getting slowly better
- Customizing _id should now fully work (_id won't be generated automatically if it is customized)
- Added display.bodyHeader message db options
- Can now customize actions on "changed", "modified", "setCallback" and "created" input events
- Added possibility to get choices from some key in another database entry
- Fixed bugs with choices methods, and shouldn't have duplicate suggestions anymore
- Fixed bug in notifier of entry modification state (would sometimes show as modified when it wasn't) (sorting of keys in objects is now recursive)
- Fixed bug with visiting modified database, should now open refreshed database as the button says
- Many more bug fixes and small improvements

## Version 1.7.2
- Fixed searching broken by last version

## Version 1.7.1
- Fixed login dialog loops when server is offline, allow users to work offline
- Action and graphify methods now also support custom methods
- Integer inputs now support custom steps
- Special "organize" mode can now be opened only if sorting is enabled
- Many other small bug fixes and improvements

## Version 1.7.0
- Fixed multi and suggest inputs wrong value being removed when clicking the cross icon
- Improved graphing abilities, allow to ignore undefined values, added more options to handle graph colors, made graphs display responsive
- Added possibility to have hidden databases
- Reworked keyboard shortcuts system, should have fixed quite a few little bugs
- Keyboard shortcuts are now customizable
- Added the possibility to create "data" entries in types database, they allow to store data that may be useful to types and methods
- Added onStart db method, that let's you run some customized code just when a db is ready, before it refreshes it's display
- Improved display of differences between added and removed sections of entries (both when editing a single entry and making mass modifications)
- Added organize special mode, to more conveniently reorder, move and multi-select displayed entries, to switch to organize mode, long press any entry in list
- Many other small bug fixes and improvements

## Version 1.6.5
- Improved recurring events basic methods

## Version 1.6.4
- Small bug fixes in some dependencies (like sorting databases by date which wasn't properly working)

## Version 1.6.3
- Added possibility to customize grouping intertitles display
- Added `subject` and `monitoring` types and some methods for them, you can use them to monitor subjects on which you work, (to use this new functionality, create a new database with those two types of entries, and customize the database display using `subjectsAndMonitoring` method for `sorting`, `grouping` and `groupingCustomDisplay`) — ⚠️ be aware that the new subjects monitoring database system that these entries propose is not yet stable and may be modified, future updates could require manual modifications/updates of the created entries
- Added method letting users do multiple processing to attached images
- Use a more consistent dialog for displaying graphs
- Many other small bug fixes and improvements

## Version 1.6.2
- When editing a type, prefilling view fields from edit keys now also prefills labels
- For basic system types, when creating an entry, a timestamp will automatically be added in meta
- Use a nicer default icon for entries, and made it easier to customize it per database
- Automatically reopen current folder and opened windows, on db reload
- Corrected a bug when removing databases (in dato autosync version, it would show login popup in loop)
- Display the global synchronization status indicator in the navigation panel, for better visibility
- It's now possible to consult the last shown notifications, from the navigation panel
- Improved a lot `anyAttachments` files previewing method
- And other small bug fixes and improvements

## Version 1.6.1
- Added missing monospace font
- When editing a type, made it easy to prefill view fields from previously chosen edit keys
- Saving an entry now does not necessarily closes it (you can chose what to do)
- Viewing an entry already opened will not reopen it, but show the relevant window
- View (and edit) windows now update automatically when entry is modified
- Added possibility to easily edit system types, so that it's possible to customize them or use them as base for making new types
- Simplified global settings dialog, removed settings not relevant anymore
- In types and databases, it's now possible to make display of custom buttons conditional
- Added support for custom methods, for all functions available in "edit" and "events" type's sections
- And many other small bug fixes and improvements

## Version 1.6.0
- Created this changelog file
- Display changelog file in dato app (in "?" > "credits and changelog")
- Added button to force refreshing dato app from last server version (the button is in "?" > "credits and changelog")
- In file input, added the possibility to upload files directly from a url
- Suggest-like inputs choices can now be lazy loaded (=> icons and countries now load only when opened, and with a spinner)
- Added new "contact" system type (making it easy to create address book databases)
- Renamed "toutdoux" system type to "todo"
  ⚠️ This is a breaking change, you should modify all your "toutdoux" entries manually: change the value of the "type" key from "toutdoux" to "todo"
- Improved "location" and "bookmark" system types
- When editing a type, suggest edit keys when choosing view keys
- Added a new method: `display.color.randomColorFromAnyValue` which lets you color your entries randomly based on their content
- Rewrote `wikipediaFetcher` utility from scratch. Now you can autofill entries with any type of content from wikipedia.
  ⚠️  The old method  `fillFromWikipedia` has been renamed to `fillFromWikipedia_usingPreset`, this is a breaking change. If you were using this method, you should manually rename it in the types using it.
- Made it possible to add some custom buttons to entries in database list-like displays
- Added the possibility to automatically pre-fill entries in dato using bookmarklet links
- And many other small fixes and UI/UX improvements
